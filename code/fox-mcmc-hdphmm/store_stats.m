% function S = store_stats(S,n,settings,stateSeq_n,dist_struct_n,theta_n,hyperparams_n)
% Store statistics into structure S and save if rem(n,saveEvery) = 0
function S = store_stats(S, n, settings, stateSeq_n, dist_struct_n, theta_n, ...
                         hyperparams_n)

sF = settings.saveEveryLogScaleFactor;
if (n >= settings.saveEvery .^ sF)
    settings.saveEvery = settings.saveEvery .^ sF;
end

doSave = (rem(n, settings.saveEvery) == 0) || (n <= 4) || (n == 8);
doSaveFinale = strcmp(S,'final') && ~doSave;
if  (doSave || doSaveFinale)
    if isfield(settings, 'startTime')
        elapsedTime = toc(settings.startTime);
    else
        elapsedTime = 0;
    end
    fid = fopen(fullfile(settings.saveDir, 'times-saved-params.txt'), 'a');
    fprintf(fid, sprintf('%.3f\n', elapsedTime));
    fclose(fid);

    N = length(stateSeq_n);
    K = settings.Kz;
    zHatBySeq = cell(N,1);
    for ii = 1:N
      T = length(stateSeq_n(ii).z);
      
      % Save to int format, zero index!
      zHatBySeq{ii,1} = int32(reshape(stateSeq_n(ii).z, T, 1))-1;
    end
    % Not really the MAP, but a posterior sample.
    % Just trying not to mess with naming conventions so much.
    outputfilename = sprintf('Lap%08.3fMAPStateSeqs.mat', n);
    outputfilepath = fullfile(settings.saveDir, outputfilename);
    save(outputfilepath, 'zHatBySeq', 'K');

    %% Run alignment via python!
    PYEXE = '/contrib/projects/EnthoughtPython/epd64/bin/python';
    if ~exist(PYEXE, 'file')
        PYEXE = '/usr/bin/python';
    end

    CMD = [PYEXE ' ' getenv('XHMMROOT') ...
           'code/MakeAlignedStateSeqs.py ' outputfilepath]; 
    fprintf('COMMAND\n%s\n', CMD);
    [status, result] = system(CMD);
    disp(['STATUS: ' num2str(status)]);
    disp(result);
    
    %% Save snapshot of parameters!
    HMMParamSnapshot = struct();
    HMMParamSnapshot.transPi = dist_struct_n.pi_z;
    HMMParamSnapshot.initPi = dist_struct_n.pi_init;
    
    D = size(theta_n.invSigma, 1);
    K = size(theta_n.invSigma, 3);
    
    Sigma = zeros(K, D, D);
    A = zeros(K, D, D);
    Mu = zeros(K, D);
    for k = 1:K
        Sigma(k,:, :) = theta_n.invSigma(:, :, k, 1) \ eye(D);
        if isfield(theta_n, 'A')
            A(k, :, :) = theta_n.A(:, :, k, 1);
            HMMParamSnapshot.A = A;
        else
            Mu(k, :) = theta_n.mu(:, k, 1);
            HMMParamSnapshot.Mu = Mu;
        end
    end
    HMMParamSnapshot.Sigma = Sigma;
    
    outputfilename = sprintf('Lap%08.3fHMMParamSnapshot.mat', n);
    save(fullfile(settings.saveDir, outputfilename), ...
        '-struct', 'HMMParamSnapshot');
end

end

% % If we are at a storing iteration:
% if rem(n,settings.storeEvery)==0 & n>=settings.saveMin
%     % And if we are at a mode-sequence storing iteration:
%     if rem(n,settings.storeStateSeqEvery)==0
%         % Store all sampled mode sequences:
%         for ii=1:length(stateSeq_n)
%             S.stateSeq(S.n,ii) = stateSeq_n(ii);
%         end
%         % Increment counter for the mode-sequence store variable:
%         S.n = S.n + 1;
%     end
%     % Store all sampled model parameters:
%     S.dist_struct(S.m) = dist_struct_n;
%     S.theta(S.m) = theta_n;
%     S.hyperparams(S.m) = hyperparams_n;
%     % Increment counter for the regular store variable:
%     S.m = S.m + 1;
%
% end

% % If we are at a saving iteration:
% if rem(n,settings.saveEvery)==0
%
%     % Save stats to specified directory:
%     if isfield(settings,'filename')
%         filename = strcat(settings.saveDir,'/',settings.filename,'iter',num2str(n),'trial',num2str(settings.trial));    % create filename for current iteration
%     else
%         filename = strcat(settings.saveDir,'/HDPHMMDPstats','iter',num2str(n),'trial',num2str(settings.trial));    % create filename for current iteration
%     end
%
%     save(filename,'S') % save current statistics
%
%     % Reset S counter variables:
%     S.m = 1;
%     S.n = 1;
%
%     display(strcat('Iteration: ',num2str(n)))
%
% end
