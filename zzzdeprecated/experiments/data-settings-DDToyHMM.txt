N 32
Ktrue 8
maxT 1000
BNPYDATADIR $XHMMROOT/datasets/$dataName/

nBatch 8
nLap 2000
nLap_test 5

saveEarly 10
saveEvery 10

obsModelName Gauss
ECovMat eye
sF 1.0
