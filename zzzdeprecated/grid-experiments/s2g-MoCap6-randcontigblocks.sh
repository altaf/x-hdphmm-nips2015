## Script for launching jobs on the grid.
# Usage
# --------------
# s2g____.sh <no args>
#    print out list of jobs that will be run
# s2g____.sh launch
#    send all jobs to the grid

dataName='MoCap6'
export jobName='Wed'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE='liv'
NTASK=1-10

export aModel=HDPHMM
export obsModel=AutoRegGauss
export ECovMat=diagcovfirstdiff
export sF=0.5
export VMat=same
export MMat=eye
export nBatch=1
export deleteFailLimit=10
export deleteStartLap=10
export mergeStartLap=5
export initname='randcontigblocks'
export initBlockLen=20
export learnRate=aa

for hmmKappa in 300
do
  export hmmKappa=$hmmKappa
  echo "-------------------- hmmKappa $hmmKappa | initBlockLen $initBlockLen"

for K in 32 64
do

# Remember: Need to run with fixed before sampler
#for alg in fixed delmerge stoch
for alg in sampler
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'runserial' ]]; then
    for n in `seq 4 10`; do
      CMD="../Run_$aModel$alg.sh $dataName $jobName $n $initname $K $obsModel $ECovMat $sF $nBatch"
      eval $CMD
    done
  else
    echo $CMD
  fi
  echo " "

done
done
done
exit
