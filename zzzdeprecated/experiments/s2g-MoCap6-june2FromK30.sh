for K in 30 
do

for hmmKappa in 100
do

for creationProposalName in bisectGrownBlocks #randBlocks dpmixture #randBlocks bisectGrownBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

if [[ $creationProposalName == 'dpmixture' ]]; then
    creationKfresh_early=4
    creationKfresh_late=2
else
    creationKfresh_early=2
    creationKfresh_late=2
fi

python LaunchRun.py \
  --nickName finalbaselines \
  --dataName MoCap6 \
  --nBatch 6 \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 5 \
  --creationProposalName $creationProposalName \
  --creationLapDelim_early 50 \
  --creationLapDelim_late 200 \
  --nLap 1000 \
  --minLaps 400 \
  --minBlockSize 10 \
  --maxBlockSize 100 \
  --growthBlockSize 25 \
  --creationNumProposal_early 5 \
  --creationNumProposal_late 1 \
  --creationKfresh_early $creationKfresh_early \
  --creationKfresh_late $creationKfresh_late \
  --nRefineIters 3 \
  --nGlobalIters 1 \
  --nGlobalItersBigChange 5 \
  --queue short \
  $*

done
done
done
done
