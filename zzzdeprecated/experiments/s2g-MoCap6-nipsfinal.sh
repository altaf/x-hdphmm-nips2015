for K in 1 10 
do

for hmmKappa in 100
do

for creationProposalName in randBlocks bisectExistingBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --nickName nipsfinal \
  --dataName MoCap6 \
  --nBatch 6 \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 5 \
  --creationProposalName $creationProposalName \
  --creationLapDelim_early 50 \
  --creationLapDelim_late 200 \
  --nLap 1000 \
  --minLaps 400 \
  --creationNumProposal_early 10 \
  --creationNumProposal_late 2 \
  --nRefineIters 3 \
  --nGlobalIters 1 \
  --nGlobalItersBigChange 5 \
  $*

done
done
done
done
