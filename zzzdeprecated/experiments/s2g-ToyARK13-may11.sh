for K in 50 100
do

for hmmKappa in 0 100
do

#for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge 
#for algName in bnpyHDPHMMstoch foxHDPHMMsampler
for algName in bnpyHDPHMMfixordermemo
do

python LaunchRun.py \
  --dataName ToyARK13 \
  --obsModelName AutoRegGauss \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 8 \
  --launchCode $1 \
  --nickName may11

done
done
done
