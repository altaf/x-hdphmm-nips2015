creationProposalName='none'
for meetingNum in 16 #11 21 # `seq 1 21`
do

echo "--------------------- meetNum $meetingNum"

for K in 25
do

for hmmKappa in 100
do

#for creationProposalName in bisectGrownBlocks #randBlocks
#do

#for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge 
#for algName in bnpyHDPHMMcreateanddestroy
for algName in foxHDPHMMsampler
do

python LaunchRun.py \
  --dataName SpeakerDiar \
  --meetingNum $meetingNum \
  --creationProposalName $creationProposalName \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 10 \
  --nickName longerfinal \
  --minBlockSize 10 \
  --maxBlockSize 500 \
  --creationLapDelim_early 5 \
  --creationLapDelim_late 50 \
  --creationNumProposal_early 5 \
  --creationNumProposal_late 2 \
  $*

#done
done
done
done
done
