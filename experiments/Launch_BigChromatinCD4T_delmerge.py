#!/usr/bin/python

import subprocess

CMD = """
python LaunchRun.py \
 --dataName BigChromatinCD4T \
 --K $K \
 --hmmKappa $hmmKappa \
 --algName $algName \
 --nickName nipsexperiment \
 --nTask 2 \
"""

for hmmKappa in [100]:
    for K in [50, 100]:
        for algName in ['bnpyHDPHMMdelmerge']:

            # Execute the command
            curCMD = CMD + ""
            curCMD = curCMD.replace('$K', str(K))
            curCMD = curCMD.replace("$algName", algName)
            curCMD = curCMD.replace("$hmmKappa", str(hmmKappa))
            proc = subprocess.Popen(curCMD.split(), shell=False)
            proc.wait()

