if [[ -z $saveEvery ]]; then
  export saveEvery="10"
fi
if [[ -z $printEvery ]]; then
  printEvery=1
fi
if [[ -z $traceEvery ]]; then
  traceEvery=1
fi

if [[ $runCode != "launch" ]]; then
  jobName="$jobName-draft"
fi

joblongname="$jobName-alg=$alg"
isStochAlg=`python -c "print '$alg'.count('stoch')"`
if [[ -n $learnRate && $isStochAlg -gt 0 ]]; then
  joblongname="$joblongname-learnRate=$learnRate"
fi

if [[ -n $hmmKappa ]]; then
  joblongname="$joblongname-hmmKappa=$hmmKappa"
fi
if [[ -n $ECovMat ]]; then
  joblongname="$joblongname-lik=$obsModel-ECovMat=$ECovMat-sF=$sF"
fi
if [[ -n $VMat && $obsModel == "AutoRegGauss" ]]; then
  joblongname="$joblongname-VMat=$VMat-MMat=$MMat"
fi

if [[ -n $initname ]]; then
  if [[ $initname == 'truelabels' ]]; then
    if [[ -z $Ktrue ]]; then
      echo "Ktrue UNDEFINED"
      exit
    fi
    export Kinit=$Ktrue
    joblongname="$joblongname-truelabels-Kinit=$Ktrue"
  elif [[ $initname == 'repeattruelabels' ]]; then
    if [[ -z $Ktrue ]]; then
      echo "Ktrue UNDEFINED"
      exit
    fi
    Kinit=`python -c "print 2*$Ktrue"`
    export Kinit=$Kinit
    joblongname="$joblongname-repeattruelabels-Kinit=$Kinit"
  else
    joblongname="$joblongname-$initname"
  fi
fi
if [[ -n $initBlockLen ]]; then
  if [[ $initname == 'randcontigblocks' || $initname == 'sacbLP' ]]; then
    joblongname="$joblongname-initBlockLen=$initBlockLen"
  fi
fi

isSacb=`python -c "print '$initname'.count('sacb')"`
if [[ -n $K && $K -gt 0 ]]; then
  if [[ $isSacb -gt 0 ]]; then
    joblongname="$joblongname-Kmax=$K"
  else
    joblongname="$joblongname-Kinit=$K"
  fi
fi

if [[ -n $nBatch ]]; then
  joblongname="$joblongname-nBatch=$nBatch"
fi

algHasMerge=`python -c "print '$alg'.count('merge')"`
if [[ -n $mergeStartLap && $algHasMerge -gt 0 ]]; then
  joblongname="$joblongname-mergeStartLap=$mergeStartLap"
fi

export joblongname
export taskoutpath="$BNPYOUTDIR/$dataName/$joblongname/$taskid"

export BNPYOutputPrefs=" \
  --printEvery $printEvery \
  --traceEvery $traceEvery \
  --saveEvery $saveEvery \
  "

if [[ $dataPath == $dataName ]]; then
 export BNPYOutputPrefs="$BNPYOutputPrefs \
        --customFuncPath $HOME/git/bnpy2/bnpy/learnalg/extras/XViterbi.py \
        "
fi
