global dataName
dataName = 'DDToyHMM'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 100]
  Kticks = [8, 25, 50, 75, 100]
  Laplims = [-2, 200]
  Lapticks = [1, 10, 100]
  Hlims = [-.025, 0.8]
  Hticks = [0, 0.2, 0.4, 0.6]
  ELBOlims = [-1.65, -1.4]
  ELBOticks = [-1.65, -1.6, -1.55, -1.5, -1.45]

  # Size of state seq plots
  ZW = 12 
  ZH = 3.5
  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks, 
                 ELBOlims, ELBOticks,
                 ZW, ZH)

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    C = [
        [166,206,227], # blue
        [31,120,180],
        [178,223,138], # green
        [51,160,44],
        [253,191,111], # orange
        [255,127,0],
        [202,178,214], # purple
        [106,61,154],
        ]
    C = np.asarray(C)/255.0;
    L = 100
    grayVals = np.linspace(0.15, 0.55, L)
    for ell in xrange(L):
        grayColor = np.asarray([grayVals[L-ell-1], 0, 0])
        #grayColor = grayVals[L-ell-1] * np.ones(3)
        C = np.vstack([C, grayColor[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap


def getColorMap():
  ShadesOfRed = [
    '#fee5d9',
    '#fcae91',
    '#fb6a4a',
    '#fb6a4a',
    '#cb181d',
    ]
  ColorKeys = ['K=1', 'K=4', 'K=16', 'K=64']
  ColorMap = OrderedDict()
  for keyPos, key in enumerate(ColorKeys):
      ColorMap[key] = ShadesOfRed[keyPos]
  return ColorMap

def getStyleMap():
  StyleMap = dict([('K', '-')])
  return StyleMap

algVals = "bnpyHDPHMMcreateanddestroy"
suffix = '-lRateDelay=1-lRatePower=0.51'
def makeJobKeyPathMap(
        KVals='1,4,16,64',
        kappaVals='100',
        creationProposalNameVals='mixture'):
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'create/delete/merge-K=%s'
    path = "may24-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=%s-"
    path += "K=%s-initname=randcontigblocks-nBatch=8"
    path += "-creationProposalName=%s"
    path += "-lik=Gauss-ECovMat=eye-sF=1.0"
    for cpName in creationProposalNameVals.split(','):
        for kappa in kappaVals.split(','):
            for K in KVals.split(','):
                jobpath = path % (kappa, K, cpName)
                jobkey = key % (K)

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

def getSubsetByName(JJ, name):
    JJ2 = OrderedDict()
    for key, path in JJ.items():
        if key.count(name):
            JJ2[key] = path
    return JJ2
