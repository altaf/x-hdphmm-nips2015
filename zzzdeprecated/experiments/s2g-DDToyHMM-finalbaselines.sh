for K in 50 100
do

for hmmKappa in 100 #0 100
do

#for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge
#for algName in bnpyHDPHMMstoch
for algName in foxHDPHMMsampler
do

python LaunchRun.py \
  --dataName DDToyHMM \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 10 \
  --launchCode $1 \
  --nickName finalbaselines \
  
done
done
done
