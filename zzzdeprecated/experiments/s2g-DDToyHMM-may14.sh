for K in 50 100
do

for hmmKappa in 100
do

for algName in bnpyHDPHMMmerge #bnpyHDPHMMdelmerge bnpyHDPHMMmergepar #foxHDPHMMsampler #bnpyHDPHMMmemo bnpyHDPHMMdelmerge
do

python LaunchRun.py \
  --dataName DDToyHMM \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 4 \
  --nWorkers 4 \
  --launchCode $1 \
  --nickName may14 \
  --nBatch 4
done
done
done
