
if [[ $learnRate == "aa" ]]; then
  export rhodelay=1
  export rhoexp=0.501
elif [[ $learnRate == "ba" ]]; then
  export rhodelay=10
  export rhoexp=0.501
elif [[ $learnRate == "ca" ]]; then
  export rhodelay=100
  export rhoexp=0.501
elif [[ $learnRate == "cc" ]]; then
  export rhodelay=100
  export rhoexp=0.9
else
  echo "UNKNOWN LEARNING RATE"
  exit
fi

export BNPYLearnRatePrefs="\
  --rhodelay $rhodelay \
  --rhoexp $rhoexp \
  "
