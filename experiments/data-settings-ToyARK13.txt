N 52
Ktrue 13
maxT 800
BNPYDATADIR $XHMMROOT/datasets/$dataName/

nBatch 13
nLap 3000
nLap_test 5

saveEvery 10
saveEarly 10

obsModelName AutoRegGauss
ECovMat eye
sF 0.1
VMat eye
sV 0.1
MMat zero
