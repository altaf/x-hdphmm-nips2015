#!/bin/bash
#$ -S /bin/bash
# ------ set working directory
#$ -cwd 
#$ -l vf=2G
# ------ attach environment variables
#$ -v HOME -v PATH -v PYTHONPATH -v OMP_NUM_THREADS
#$ -v BNPYDATADIR -v BNPYOUTDIR -v BNPYROOT -v XHMMROOT
#$ -v runCode -v hmmKappa -v VMat -v MMat -v initBlockLen 
# ------ send to particular queue
#$ -o /data/liv/x-hmm/logs/$JOB_ID.$TASK_ID.out
#$ -e /data/liv/x-hmm/logs/$JOB_ID.$TASK_ID.err

echo ">>>>>>>>>>>>>>>>>>>>>>>>> This is $0"
if [[ -z $1 ]]; then
  echo "Usage:"
  echo "./Run____.sh DatasetName myjobname taskid initname K nBatch"
  exit;
fi
if [[ -z $XHMMROOT ]]; then
  echo "Required var $XHMMROOT undefined. Exiting."
  exit;
fi
cd $XHMMROOT
source setup-env/SetupEnv-RequiredVars.sh

export alg=HDPHMMfixed
export dataName=$1
export jobName=$2
export taskid=$3
export initname=$4
export K=$5
export obsModel=$6
export ECovMat=$7
export sF=$8
export nBatch=$9

export onGrid=0
if [[ -n $SGE_TASK_ID ]]; then
  export taskid=$SGE_TASK_ID
  export onGrid=1
fi

source setup-env/SetupEnv-DataPrefs.sh
source setup-env/SetupEnv-ModelPrefs.sh
source setup-env/SetupEnv-OutputPrefs.sh
source setup-env/SetupEnv-AlgPrefs.sh 

CMD="$dataPath HDPHMM $obsModel moVB \
  --K $K \
  --initname $initname \
  $BNPYDataPrefs \
  $BNPYModelPrefs \
  $BNPYAlgPrefs \
  $BNPYOutputPrefs \
  --taskid $taskid \
  --jobname $joblongname \
  "


echo ">>>>>>>>>>>>>> Handing off to RunBNPYonGrid.py, with args:" 
echo $CMD
echo ''


python RunBNPYonGrid.py $CMD
