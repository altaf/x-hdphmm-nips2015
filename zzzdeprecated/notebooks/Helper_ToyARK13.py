global dataName
dataName = 'ToyARK13'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 105]
  Kticks = [25, 50, 75, 100]

  Laplims = [0, 2100]
  Lapticks = [2, 10, 100, 1000]

  Hlims = [-0.025, 0.5]
  Hticks = [0, 0.1, 0.2, 0.3, 0.4]
  # Size of state seq plots
  ZW = 12
  ZH = 3.5

  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks,
                 ZW, ZH)

## initname=truelabels
J = dict()
prefix0 = 'Thurs-alg=HDPHMMfixed-hmmKappa=0-lik=AutoRegGauss-ECovMat=eye-sF=0.1-VMat=eye-MMat=zero-randcontigblocks-initBlockLen=20-'

prefix100 = 'Thurs-alg=HDPHMMfixed-hmmKappa=100-lik=AutoRegGauss-ECovMat=eye-sF=0.1-VMat=eye-MMat=zero-randcontigblocks-initBlockLen=20-'

J['fixed-kappa=0-Kinit=50'] = prefix0 + 'Kinit=50-nBatch=10'
J['fixed-kappa=0-Kinit=100'] = prefix0 + 'Kinit=100-nBatch=10'

J['fixed-kappa=100-Kinit=50'] = prefix100 + 'Kinit=50-nBatch=10'
J['fixed-kappa=100-Kinit=100'] = prefix100 + 'Kinit=100-nBatch=10'

"""
prefix = 'Sunday-alg=HDPHMMfixed-hmmKappa=0-lik=AutoRegGauss-ECovMat=eye-sF=0.1-VMat=eye-MMat=zero-'

# 32 is too small!
#J['fixed-rcontigblocks-Kinit=32'] = prefix + "randcontigblocks-initBlockLen=20-Kinit=32-nBatch=10"
J['fixed-rcontigblocks-Kinit=64'] = prefix + "randcontigblocks-initBlockLen=20-Kinit=64-nBatch=10"
"""

def getSubsetByName(JJ, name):
  JJ2 = OrderedDict()
  for key, path in JJ.items():
    if key.count(name):
      JJ2[key] = path
  return JJ2

def getLongRunJobPathsByKeys():
  J = getJobPathsByKeys()
  for key, path in J.items():
    if key.count('delmerge') == 0:
      path = path.replace('Thurs', 'Thurs-Longer')
    J[key] = path
  return J

def getJobPathsByKeys():
  global J
  JJ = copy.deepcopy(J)
  for key, path in JJ.items():  
    if not os.path.exists(PlotUtil.MakePath(path)):
      print 'CANNOT FIND USER-SPECIFIED PATH: ', path
      continue

    for newname in ['delmerge', 'sampler', 'stoch']:
      rkey = key.replace('fixed', newname)
      rpath = path.replace('fixed', newname)
      if newname == 'delmerge':
        rpath = rpath + '-mergeStartLap=1'
      if newname == 'stoch':
        rpath = path.replace('fixed', 'stoch-learnRate=aa')
      if not os.path.exists(PlotUtil.MakePath(rpath)):
        print 'CANNOT FIND AUTO-GENERATED PATH: ', rpath
        continue
      JJ[rkey] = rpath
  # Sort by keys
  return OrderedDict(reversed(sorted(JJ.items())))

def getColorMap():
  ColorMap = OrderedDict()
  ColorMap = dict()
  ColorMap['fixed'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  ColorMap['truelabels'] = 'y'
  ColorMap['repeattrue'] = 'm'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('50', '--'), ('100','-'),
                  ])
  return StyleMap
