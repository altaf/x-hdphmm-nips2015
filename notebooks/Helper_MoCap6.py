global dataName
dataName = 'MoCap6'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

# Define colormap for visualizing true states
Colors = [
        [166,206,227],
        [31,120,180],
        [178,223,138],
        [51,160,44],
        [251,154,153],
        [255,127,0],
        [253,191,111],
        [227,26,28],
        [202,178,214],
        [106,61,154],
        [255,255,153],
        [177,89,40],
        ]

# Define value ranges used for experiments
kappaVals = ['100']
KVals = ['30', '60']
KVals_Birth = ['1']
algNameVals = [
  'bnpyHDPHMMstoch',
  'foxHDPHMMsampler',
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
suffix = '-lRateDelay=1-lRatePower=0.51'


def setUp():
  Klims = [-0.5, 65]
  Kticks = [0, 20, 40, 60]

  Laplims = [0, 2100]
  Lapticks = [1, 10, 100, 1000]

  Hlims = [-0.03, 0.85]
  Hticks = [0, 0.2, 0.4, 0.6, 0.8]

  ELBOlims = [-2.85, -2.0]
  ELBOticks = [-2.8, -2.6, -2.4, -2.2]

  # Size of state seq plots
  ZW = 8
  ZH = 4.5 # Dont shrink this. Text gets cut off!

  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  J = makePathMap()
  J.update(makePathMap_ForBirths())
  return J


def getStateSeqColorMap(nErrorStates=20):
    ''' Create colormap with entry for each of the 8 true states.

    Extraneous state indices are assigned to different shades of red.

    Returns
    -------
    Cmap : ListedColormap
    '''
    from matplotlib.colors import ListedColormap
    C = np.asarray(Colors)/255.0;
    L = int(np.ceil(nErrorStates/2.0))
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        g = grayVals[L-ell-1]
        shadeOfGray = np.asarray([g, g, g])
        C = np.vstack([C, shadeOfGray[np.newaxis,:]] )
    L = nErrorStates/2
    grayVals = np.linspace(0.2, 0.9, L)
    for ell in xrange(L):
        g = grayVals[L-ell-1]
        shadeOfGray = np.asarray([g, g, g])
        C = np.vstack([C, shadeOfGray[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['delmerge'] = 'r'
    ColorMap['birth'] = 'm'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=1'] = '-'
    StyleMap['K=30'] = '--'
    StyleMap['K=60'] = '-'
    return StyleMap

def makePathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "memo Sticky=50 K=50"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s Sticky=%s K=%s'
    path = "nipsexperiment-alg=%s-lik=AutoRegGauss-hmmKappa=%s"
    path += "-ECovMat-diagcovfirstdiff-sF=0.5-VMat=same-MMat-eye"
    path += "-K=%s-initname=randcontigblocks-nBatch=6"
    for algName in algNameVals:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (algName, kappa, K)
                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makePathMap_ForBirths():
    J = OrderedDict()
    key = 'birth Sticky=%s K=%s'
    path = "nipsexperiment-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=%s-"
    path += "K=%s-initname=randcontigblocks-nBatch=6"
    path += "-creationProposalName=bisectGrownBlocks"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff"
    path += "-sF=0.5-VMat=same-MMat=eye"
    for kappa in kappaVals:
        for K in KVals_Birth:
                jobpath = path % (kappa, K)
                jobkey = key % (kappa, K)
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makeLegendInOwnFigure(
        Jdict,
        names = ['stoch', 'sampler', 'memo', 'delete,merge', 'birth,delete,merge'],
    ):
    ''' Create legend for trace plots, in separate matplotlib figure.
    '''
    # Split up each name into original name (key in Jdict) and legend name
    origNames = list()
    legendNames = list()
    for name in names:
        if name.count(":") > 0:
            fields = name.split(":")
            origNames.append(fields[0])
            legendNames.append(fields[1])
        else:
            origNames.append(name)
            legendNames.append(name)            
    # Build job dict with one entry for each of the specified names
    J2 = OrderedDict()
    for name in origNames:
        Jmatch = getSubsetByName(Jdict, name, keepName=1)
        if len(Jmatch.keys()) == 0:
            raise ValueError("Cannot find key %s in provided job path dict" % (
                name))
        firstkey = Jmatch.keys()[0]
        Jsingle = dict()
        Jsingle[firstkey] = Jmatch[firstkey]
        J2.update(Jsingle)
    # Make trace plot with these jobs only
    PlotUtil.plotHammingDist(J2, loc=None, xscale='log')
    # Grab current axes of this trace plot
    axH = PlotUtil.pylab.gca();
    # Set all line handles to wide, solid lines
    lineHandles, lineLabels = axH.get_legend_handles_labels()
    for ii in range(len(lineHandles)):
        lineHandles[ii].set_linewidth(6)
        lineHandles[ii].set_linestyle('-')
    # Remove lines from the plot entirely, so its just the legend
    axH.axis('off')
    for ii in range(len(axH.lines)):
        axH.lines.pop()
    # Show the legend
    axH.legend(lineHandles, legendNames)

def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

