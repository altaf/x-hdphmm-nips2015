for K in 50 100
do

for hmmKappa in 0 100
do

for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge bnpyHDPHMMfixordermemo
#for algName in bnpyHDPHMMstoch foxHDPHMMsampler
do

python LaunchRun.py \
  --dataName DDToyHMM \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 4 \
  --launchCode $1 \
  --nickName may11 \
  
done
done
done
