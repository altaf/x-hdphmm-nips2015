allocModelName HDPHMM
obsModelName Gauss
bnpy_algName moVB

startAlpha 10
alpha 0.5
gamma 10
hmmKappa 0

ECovMat covdata
sF 0.1
VMat eye
sV 0.1
MMat zero
sM 1.0
kappa 1e-7

lam1 0.1
lam0 0.1

jobname_if_$obsModelName=Gauss $nickName-alg=$algName-lik=$obsModelName-hmmKappa=$hmmKappa-ECovMat-$ECovMat-sF=$sF-K=$K-initname=$initname-nBatch=$nBatch
jobname_if_$obsModelName=AutoRegGauss $nickName-alg=$algName-lik=$obsModelName-hmmKappa=$hmmKappa-ECovMat-$ECovMat-sF=$sF-VMat=$VMat-MMat-$MMat-K=$K-initname=$initname-nBatch=$nBatch
jobname_if_$obsModelName=Bern $nickName-alg=$algName-lik=$obsModelName-lam1=$lam1-lam0=$lam0-hmmKappa=$hmmKappa-K=$K-initname=$initname-nBatch=$nBatch

algName bnpyHDPHMMdelmerge
nickName DEFAULT
dataName DEFAULT
customFuncPath $HOME/git/bnpy2/bnpy/learnalg/extras/XViterbi.py

initname randcontigblocks
K 25
K_test 3

saveEarly 10
saveEveryLogScaleFactor 2
saveEvery 10
printEvery 1
traceEvery 1
doSaveObsModel 1

convergeThr 0.001
doFullPassBeforeMstep 1
doMemoizeLocalParams 0
nLap 100
nLap_test 5
minLaps 100
minLaps_test 1

limitMemoryLP 1

moves shuffle,merge,delete

mergeStartLap 5
mergePairSelection wholeELBObetter
mergePerLap 25
mergeNumStuckBeforeQuit 25
mergeScoreRefreshInterval 10
mergeNumExtraCandidates 0
mergeLogVerbose 0

deleteStartLap 5
dtargetMaxSize 10
deleteNumStuckBeforeQuit 25
deleteFailLimit 5
dtargetMinCount 0.01

