global dataName
dataName = 'DDToyHMM'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

def setUp():
  Klims = [-0.5, 105]
  Kticks = [8, 20, 40, 60, 80, 100]

  Laplims = [0, 2500]
  Lapticks = [1, 10, 100, 1000]

  Hlims = [-0.03, 0.85]
  Hticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]

  ELBOlims = [-1.65, -1.4]
  ELBOticks = [-1.6, -1.55, -1.5, -1.45]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5 # Dont shrink this. Text gets cut off!
  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  return makeKeyPathMap()

BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet[1:3] + GreenSet[1:3] + OrangeSet[1:3] + PurpleSet[1:3]
    C = np.asarray(Colors)/255.0;
    L = 4
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['delmerge'] = 'r'
    ColorMap['randBlocks'] = 'c'
    ColorMap['bisect'] = 'm'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=50'] = '--'
    StyleMap['K=100'] = '-'
    StyleMap['bisect'] = '-'
    return StyleMap

kappaVals = ['0', '100']
KVals = ['50', '100']

algNameVals = [
  'bnpyHDPHMMstoch',
  'foxHDPHMMsampler',
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
suffix = '-lRateDelay=1-lRatePower=0.51'

def makeKeyPathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'K=%s alg=%s Sticky=%s'
    path = "finalbaselines-alg=%s-lik=Gauss-hmmKappa=%s"
    path += "-ECovMat-eye-sF=1.0"
    path += "-K=%s-initname=randcontigblocks-nBatch=8"
    for algName in algNameVals:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (K, algName, kappa)

                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp.replace('nBatch=6', 'nBatch=1')
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

Kvals = [
  '1', '10', '50']

creationProposalNameVals = [
  'bisectExistingBlocks',
  ]
def makeKeyPathMap_ForBirths():
    J = OrderedDict()
    key = 'K=%s %s Sticky=%s'
    path = "nipsfinal-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=%s-"
    path += "K=%s-initname=randcontigblocks-nBatch=8"
    path += "-creationProposalName=%s"
    path += "-lik=Gauss-ECovMat=eye-sF=1.0"
    for cpName in creationProposalNameVals:
      for kappa in kappaVals:
        for K in Kvals:
                jobpath = path % (kappa, K, cpName)
                jobkey = key % (K, cpName.replace('Existing', ''), kappa)
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
    
def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

