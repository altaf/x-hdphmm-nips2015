hmmKappa=100
initname='randcontigblocks'

for creationProposalName in bisectExistingBlocks randBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

for K in 1 
do

python LaunchRun.py \
  --dataName BigChromatinCD4T \
  --K $K \
  --creationProposalName $creationProposalName \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 2 \
  --nickName may31 \
  --nRefineIters 1 \
  --mergePerLap 10 \
  --dtargetMaxSize 10 \
  --deleteStartLap 2 \
  --mergeStartLap 4 \
  $*
done
done
done
