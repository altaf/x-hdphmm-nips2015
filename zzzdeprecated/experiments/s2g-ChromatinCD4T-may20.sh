# New: expanded delete max size to full dataset
# New: made contigblocks longer (size=50)

for hmmKappa in 100 0
do

for algName in bnpyHDPHMMdelmerge bnpyHDPHMMmemo
do

for K in 50 100
do
 
initname='randcontigblocks'

python LaunchRun.py \
  --dataName ChromatinCD4T \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 2 \
  --launchCode $1 \
  --nickName may20

done
done
done
