'''
Goal
------
Make MAT file with fields
* nDoc
* doc_range
* X
* Xprev (if present)
'''

import bnpy
import MoCap6

def Make_data_struct():
  Data = MoCap6.get_data()
  from IPython import embed; embed()
  Data.save_to_mat('HMMdataset.mat')
  

if __name__ == '__main__':
  Make_data_struct()
