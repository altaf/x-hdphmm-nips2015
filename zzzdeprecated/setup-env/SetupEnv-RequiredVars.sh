if [[ -z $BNPYOUTDIR ]]; then
  echo "Required var BNPYOUTDIR undefined";
  exit;
fi

if [[ -z $BNPYROOT ]]; then
  echo "Required var $BNPYROOT undefined";
  exit;
fi

if [[ -z $XHMMROOT ]]; then
  echo "Required var $XHMMROOT undefined";
  exit;
fi

echo "BNPYROOT: $BNPYROOT"
echo "XHMMROOT: $XHMMROOT"
echo "BNPYOUTDIR: $BNPYOUTDIR"

export FOXMCMCCODEPATH=$XHMMROOT/competitor-code/fox-mcmc-hdphmm/
