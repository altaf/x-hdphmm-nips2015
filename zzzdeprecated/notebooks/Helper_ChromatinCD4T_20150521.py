global dataName
dataName = 'ChromatinCD4T'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 105]
  Kticks = [25, 50, 75, 100]

  Laplims = [0, 120]
  Lapticks = [2, 10, 100]

  Hlims = None
  Hticks = None

  ELBOlims = None
  ELBOticks = None

  # Size of state seq plots
  ZW = 12
  ZH = 3.5

  PlotUtil.setUp(dataName, getStateSeqColorMap(), getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks, 
                 ELBOlims, ELBOticks,
                 ZW, ZH)

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = [[127,127,127]]
    C = np.asarray(Colors)/255.0;
    L = 100
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap


def getColorMap():
  ColorMap = dict()
  ColorMap['memo'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('K=25', ':'), ('K=50','--'), ('K=100', '-')])
  return StyleMap

algVals = 'bnpyHDPHMMmemo,bnpyHDPHMMdelmerge'
suffix = '-lRateDelay=1-lRatePower=0.51'
def makeJobKeyPathMap(
        KVals='50,100',
        kappaVals='0,100',
        algVals=algVals):
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s-Sticky=%s-K=%s'
    path = "may20-alg=%s-lik=Bern-lam1=0.1-lam0=0.3"
    path += "-hmmKappa=%s-K=%s-initname=randcontigblocks-nBatch=24"

    for alg in algVals.split(','):
        for kappa in kappaVals.split(','):
            for K in KVals.split(','):
                algkey = str(alg) # copy
                algkey = algkey.replace('bnpy', '')
                algkey = algkey.replace('HDPHMM', '')
                algkey = algkey.replace('fox', '')

                jobpath = path % (alg, kappa, K)
                jobkey = key % (algkey, kappa, K)

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
def extractBlocksFromMask(mask):
    '''

    Returns
    -------
    blockSizes : 1D array of size B
    blockStarts : 1D array of size B
    '''
    mask = np.asarray(mask)
    changePts = np.asarray(np.hstack([0, 
        1+np.flatnonzero(np.diff(mask)),
        mask.size]), dtype=np.float64)
    if len(changePts) == 2:
        # Base case. No changes.
        blockStarts = np.asarray([0.])
        if mask[0] > 0:
            blockSizes = np.asarray([mask.size])
        else:
            blockSizes = np.asarray([0.])
    else:
        if mask[changePts[0]] > 0:
            chPtA = changePts[1::2]
            chPtB = changePts[0::2]
        else:
            chPtA = changePts[2::2]
            chPtB = changePts[1::2]
        chPtB = chPtB[:chPtA.size]
        blockSizes = chPtA - chPtB
        blockStarts = chPtB

    if blockStarts[0] != 0:
        blockStarts = np.hstack([0, blockStarts])
        blockSizes = np.hstack([0, blockSizes])
    return np.asarray(blockSizes, dtype=np.float64), blockStarts
    

def makeBEDFileFromSegmentation(chrData, taskpath, lapFrac, ColorMap=[[255,0,0]]):
    ''' Create .bed format file for display at genome.ucsc.edu

    Post Condition
    --------------
    File exists at <taskpath>/Lap<Prefix>Segmentation.bed
    '''
    import bnpy
    import scipy.io
    from bnpy.util.StateSeqUtil import convertStateSeq_MAT2list
    ColorMap = np.asarray(ColorMap)
    header1 = "browser position chr7:116,260,000-116,361,000"
    header2 = 'track name="CD4T segmentation test" visibility=2 itemRgb="On" useScore=1'
    chrName = chrData.fileNames[0]
    slinePattern = "chr7 0 " + str(chrData.endLoc_bp) + " state??? 1000 . 0 0"
    segfpath = os.path.join(taskpath, "Lap%08.3fMAPStateSeqs.mat" % (lapFrac))
    SaveVars = scipy.io.loadmat(segfpath)
    zBySeqMAT = SaveVars["zHatBySeq"]
    zBySeq = convertStateSeq_MAT2list(zBySeqMAT)
    for zHat in zBySeq:
        uLabels = np.unique(zHat)
        for uLoc, uID in enumerate(uLabels):
            sline = slinePattern.replace("???", str(uID))
            blockSizes, blockStarts = extractBlocksFromMask(zHat == uID)
            blockSizes *= chrData.stepSize_bp
            blockStarts *= chrData.stepSize_bp
            blockStarts[1:] += chrData.startLoc_bp

            # Must add final singleton block to mark the endLoc 
            blockSizes = np.hstack([blockSizes, 1])
            blockStarts = np.hstack([blockStarts, chrData.endLoc_bp-1])

            blockSizeStr = ",".join(['%d' % (x) for x in blockSizes])
            blockStartStr = ",".join(['%d' % (x) for x in blockStarts])
            curColorRGB = ColorMap[uLoc%ColorMap.shape[0]]
            colorStr = ",".join(['%d' % (x) for x in curColorRGB])
            sline = sline + " %s %d %s %s" % (
                colorStr, len(blockSizes), blockSizeStr, blockStartStr)
            print sline

    
def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

def getSubsetByName(JJ, name):
    JJ2 = OrderedDict()
    for key, path in JJ.items():
        if key.count(name):
            JJ2[key] = path
    return JJ2
