## Discover best obsmodel parameters for dataset

export doProfiler=1

dataName='DDToyHMM'
export jobName='profile'
export runCode=$1
export OMP_NUM_THREADS=4

for aModel in HDPHMM
do

for obsModel in Gauss
do

for alg in delmerge
do

for ECovMat in eye
do

for sF in 1
do

for initname in 'randexamplesbydist' #'kmeans'
do

for K in 32
do

for nBatch in 1
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    eval $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
done
done
done
done
done
exit
