for hmmKappa in 0 100
do

for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge
do

for initname in truelabels randexamplesbydist randcontigblocks
do
 
   if [[ $init == 'truelabels' ]]; then
      K=-1
   else
      K=100
   fi

python LaunchRun.py \
  --dataName SeqOfBinBars9x9 \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 2 \
  --launchCode $1 \
  --nickName may20

done
done
done
