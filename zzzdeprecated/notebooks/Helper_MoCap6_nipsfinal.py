global dataName
dataName = 'MoCap6'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

def setUp():
  Klims = [-0.5, 50]
  Kticks = [0, 10, 20, 30, 40]

  Laplims = [0, 520]
  Lapticks = [1, 10, 100]

  Hlims = [-0.03, 0.6]
  Hticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5]

  ELBOlims = [-2.2, -2.07]
  ELBOticks = [-2.2, -2.15, -2.1]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5
  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  return makeKeyPathMap()

BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet + GreenSet[::-1] + OrangeSet + PurpleSet[::-1]
    C = np.asarray(Colors)/255.0;
    L = 10
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    ColorMap['repeattruelabels'] = 'g'
    ColorMap['K=1'] = 'r'
    ColorMap['K=10'] = 'b'
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['randBlocks'] = '-'
    StyleMap['bisectExistingBlocks'] = '-'
    StyleMap['dpmixture'] = '-'
    return StyleMap

creationProposalNameVals = [
  'randBlocks',
  'bisectExistingBlocks',
  ]

initnameVals = [
  'K=1-initname=randcontigblocks',
  'K=10-initname=randcontigblocks',
  ]

def makeKeyPathMap(
        initnameVals=initnameVals,
        creationProposalNameVals=creationProposalNameVals):
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s proposal=%s'
    path = "nipsfinal-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "%s-nBatch=6"
    path += "-creationProposalName=%s"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye"
    for cpName in creationProposalNameVals:
        for iName in initnameVals:
                jobpath = path % (iName, cpName)
                jobkey = key % (iName.split('-')[0], cpName)

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True
