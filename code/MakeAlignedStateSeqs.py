import numpy as np
import scipy.io
import os

import bnpy
from bnpy.util.StateSeqUtil import alignEstimatedStateSeqToTruth, convertStateSeq_list2MAT, convertStateSeq_MAT2list, convertStateSeq_list2flat, convertStateSeq_flat2list, calcHammingDistance

def makeAlignedStateSeqsFromMATFile(origmatpath):
  # Figure out dataName
  dataName = origmatpath.replace(os.environ['BNPYOUTDIR'], '')
  if dataName.startswith(os.path.sep):
    dataName = dataName[1:]
  dataName = dataName.split(os.path.sep)[0]
  print 'Aligning state seqs for dataset: ', dataName

  # Load the estimated zs and convert to list format
  SVars = scipy.io.loadmat(origmatpath)
  estZlist = convertStateSeq_MAT2list(SVars['zHatBySeq'])

  # Find relabeling that aligns to true labels, and get back into MAT format
  alignedZlist, hdist = getAlignedStateSeqsAndHammingDist(dataName, estZlist)

  zHatBySeq = convertStateSeq_list2MAT(alignedZlist)

  # Save to MAT file
  outmatpath = origmatpath.replace('MAPStateSeqs', 'MAPStateSeqsAligned')
  scipy.io.savemat(outmatpath, dict(zHatBySeq=zHatBySeq), oned_as='rows')

  # Write to text files!
  outpathFields = origmatpath.split(os.path.sep)
  taskoutpath = os.path.sep.join(outpathFields[:-1])

  outtxtfile = os.path.join(taskoutpath, 'hamming-distance.txt')
  with open(outtxtfile, 'a') as f:
    f.write('%.6f\n' % (hdist))

  lap = float(outpathFields[-1][3:9])
  outtxtfile = os.path.join(taskoutpath, 'laps-saved-params.txt')
  with open(outtxtfile, 'a') as f:
    f.write('%d\n' % (lap))

  Keff = getKeff(alignedZlist)
  outtxtfile = os.path.join(taskoutpath, 'Keff-saved-params.txt')
  with open(outtxtfile, 'a') as f:
    f.write('%d\n' % (Keff))
  outtxtfile = os.path.join(taskoutpath, 'K-saved-params.txt')
  with open(outtxtfile, 'a') as f:
    f.write('%d\n' % (SVars['K']))
  print 'Done. Appended data from lap %.3f to \n  %s' % (
       lap, taskoutpath)

def getAlignedStateSeqsAndHammingDist(dataName, estZlist):
    Data, trueZflat = loadDatasetByName(dataName)
    estZflat = convertStateSeq_list2flat(estZlist, Data)
    assert trueZflat.size == estZflat.size
    alignedZflat = alignEstimatedStateSeqToTruth(estZflat, trueZflat)

    # Calc hamming distance
    hdistance = calcHammingDistance(trueZflat, alignedZflat)

    # Convert flattened alignments back to list format
    alignedZlist = convertStateSeq_flat2list(alignedZflat, Data)

    # Return the aligned sequences and the hamming distance
    return alignedZlist, hdistance

def getKeff(zList):
    ''' Calculate number of effective states from given list of Z arrays.
    
    Returns
    -------
    Keff : integer in {1, 2, ...}
    '''
    uLabels = []
    for zSeq in zList:
        uLabels = np.unique(np.hstack([uLabels, zSeq]))
    return len(uLabels)

def loadDatasetByName(dataName):
    ''' Load bnpy dataset object by name

    Returns
    -------
    Data : bnpy GroupXData object
    trueZflat : dict
        must contain field named "TrueZ" for alignment to work
    '''
    if dataName.count('SpeakerDiar'):
        meetingNum = dataName[len('SpeakerDiar'):]
        import SpeakerDiar
        Data = SpeakerDiar.get_data(meetingNum=int(meetingNum))
    else:
        dpath = os.path.join(
            os.environ['XHMMROOT'], 'datasets', dataName, 'HMMdataset.mat')
        if not os.path.exists(dpath):
            raise ValueError('Dataset not found: %s' % (dpath))
        DVars = scipy.io.loadmat(dpath)
        Data = bnpy.data.GroupXData(**DVars)
    # Make sure trueZ is really flat.
    trueZ = np.squeeze(np.asarray(
        Data.TrueParams['Z'], dtype=np.int32).copy())
    assert trueZ.ndim == 1
    return Data, trueZ

def calcLscoreFromHardSegmentation(origmatpath, doAppendTxtFile=True):
    ''' Compute variational objective from specific segmentation (as .mat file)

    Returns
    -------
    Lscore : scalar

    Post Condition
    --------------
    if doAppend, will append to evidence-saved-params.txt
    '''
    # Figure out dataName
    dataName = origmatpath.replace(os.environ['BNPYOUTDIR'], '')
    if dataName.startswith(os.path.sep):
        dataName = dataName[1:]
    dataName = dataName.split(os.path.sep)[0]

    # Load train data
    Data, TrueZ = loadDatasetByName(dataName)

    # Load the estimated zs and convert to flat 1D array
    SVars = scipy.io.loadmat(origmatpath)
    estZflat = convertStateSeq_list2flat(
        convertStateSeq_MAT2list(SVars['zHatBySeq']),
        Data)
    initLP = bnpy.init.FromTruth.convertLPFromHardToSoft(
        dict(Z=estZflat), Data)

    # Load a model with right hyperparameters
    # Requires similar run with memoized already started.
    memomatpath = origmatpath.replace('foxHDPHMMsampler', 'bnpyHDPHMMmemo')
    memomatpath = os.path.sep.join(memomatpath.split(os.path.sep)[:-1])
    print memomatpath
    model, lapQ = bnpy.ioutil.ModelReader.loadModelForLap(memomatpath, 0.0)

    # Run inference one step forward
    initLP = model.allocModel.initLPFromResp(Data, initLP)
    initSS = model.get_global_suff_stats(Data, initLP)
    model.update_global_params(initSS)
    LP = model.calc_local_params(Data)
    SS = model.get_global_suff_stats(Data, LP)
    model.update_global_params(SS)

    # Compute evidence
    Lscore = model.calc_evidence(SS=SS)
    print Lscore, '<<< Lscore'

    # Write to text files!
    if doAppendTxtFile:
        outpathFields = origmatpath.split(os.path.sep)
        taskoutpath = os.path.sep.join(outpathFields[:-1])
        outtxtfile = os.path.join(taskoutpath, 'evidence-saved-params.txt')
        with open(outtxtfile, 'a') as f:
            f.write('%.6f\n' % (Lscore))
    return Lscore, model

if __name__ == '__main__':
    print '>>> START MakeAlignedStateSeqs.py'
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('matfilepath')
    args = parser.parse_args()
    calcLscoreFromHardSegmentation(args.matfilepath)
    makeAlignedStateSeqsFromMATFile(args.matfilepath)
    print '<<< DONE  MakeAlignedStateSeqs.py'


