#!/usr/bin/python

import subprocess

CMD = """
python LaunchRun.py \
 --dataName DDToyHMM \
 --K $K \
 --hmmKappa $hmmKappa \
 --algName $algName \
 --nickName nipsexperiment \
 --nTask 10 \
"""

for hmmKappa in [0, 50]:
    for K in [50, 100]:
        for algName in ['bnpyHDPHMMdelmerge']:

            # Execute the command
            curCMD = CMD + ""
            curCMD = curCMD.replace('$K', str(K))
            curCMD = curCMD.replace("$algName", algName)
            curCMD = curCMD.replace("$hmmKappa", str(hmmKappa))
            proc = subprocess.Popen(curCMD.split(), shell=False)
            proc.wait()

