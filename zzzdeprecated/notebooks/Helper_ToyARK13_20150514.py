global dataName
dataName = 'ToyARK13'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 85]
  Kticks = [13, 25, 50, 75]

  Laplims = [0, 2100]
  Lapticks = [2, 10, 100, 1000]

  Hlims = [-0.025, 0.5]
  Hticks = [0, 0.1, 0.2, 0.3, 0.4]

  ELBOlims = [1.25, 1.45]
  ELBOticks = [1.3, 1.4]

  # Size of state seq plots
  ZW = 12
  ZH = 3.5

  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks, 
                 ELBOlims, ELBOticks,
                 ZW, ZH)
BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet + GreenSet[::-1] + OrangeSet + PurpleSet[::-1]
    Colors.append([127,127,127])
    C = np.asarray(Colors)/255.0;
    L = 100
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getColorMap():
  ColorMap = dict()
  ColorMap['memo'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('8', '-'), ('50','--'), ('100', '-')])
  return StyleMap

algVals = 'bnpyHDPHMMstoch,bnpyHDPHMMmemo,bnpyHDPHMMdelmerge'
suffix = '-lRateDelay=1-lRatePower=0.51'
def makeJobKeyPathMap(
        KVals='50,100',
        kappaVals='0,100',
        algVals=algVals):
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s-Sticky=%s-K=%s'
    path = "may11-alg=%s-lik=AutoRegGauss-hmmKappa=%s-ECovMat-eye-sF=0.1"
    path += "-VMat=eye-MMat-zero"
    path += "-K=%s-initname=randcontigblocks-nBatch=13"

    for alg in algVals.split(','):
        for kappa in kappaVals.split(','):
            for K in KVals.split(','):
                algkey = str(alg) # copy
                algkey = algkey.replace('bnpy', '')
                algkey = algkey.replace('HDPHMM', '')
                algkey = algkey.replace('fox', '')

                jobpath = path % (alg, kappa, K)
                jobkey = key % (algkey, kappa, K)

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
    
def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

def getSubsetByName(JJ, name):
    JJ2 = OrderedDict()
    for key, path in JJ.items():
        if key.count(name):
            JJ2[key] = path
    return JJ2
