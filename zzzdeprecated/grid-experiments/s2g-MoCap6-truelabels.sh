## Script for launching jobs on the grid.
# Usage
# --------------
# s2g____.sh <no args>
#    print out list of jobs that will be run
# s2g____.sh launch
#    send all jobs to the grid

dataName='MoCap6'
export jobName='Friday'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=short
NTASK=1-11

export aModel=HDPHMM
export obsModel=AutoRegGauss
export ECovMat=diagcovfirstdiff
export sF=0.5
export VMat=same
export nBatch=1
export deleteStartLap=10
export mergeStartLap=5

for hmmKappa in 300 #0 100
do
  export hmmKappa=$hmmKappa

for MMat in 'eye' #'zero'
do
  export MMat=$MMat

  echo "--------------------- hmmKappa $hmmKappa, MMat=$MMat"

# Remember: Need to run with fixed before sampler
for alg in sampler #fixed delmerge #sampler # delmerge fixed #fixed #sampler #merge delmerge
do

for initname in truelabels repeattruelabels #truelabels
do

  if [[ $initname == 'truelabels' ]]; then
    NTASKcur=1 # all runs will be the same, so just do one of them
  else
    NTASKcur=$NTASK
  fi

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname -1 $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASKcur -l $QTYPE -q "*@@liv" $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
done
exit
