for K in 1
do

for initname in repeattruelabels truelabels randcontigblocks
do

for hmmKappa in 100
do

for creationProposalName in dpmixture #randBlocks bisectExistingBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --doFullPassBeforeMstep 1 \
  --dataName MoCap6 \
  --obsModelName AutoRegGauss \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --creationProposalName $creationProposalName \
  --nTask 4 \
  --launchCode $1 \
  --nickName may27
  
done
done
done
done
done
