for hmmKappa in 0 100
do

#for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge
for algName in foxHDPHMMsampler
do

for init in 'truelabels' 'repeattruelabels' '50' '100'
do
 
  if [[ $init == 'truelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'repeattruelabels' ]]; then
     K=-1
     initname=$init
  else
     K=$init
     initname='randcontigblocks'
  fi

python LaunchRun.py \
  --dataName MoCap6 \
  --obsModelName AutoRegGauss \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 4 \
  --launchCode $1 \
  --nickName may11

done
done
done
