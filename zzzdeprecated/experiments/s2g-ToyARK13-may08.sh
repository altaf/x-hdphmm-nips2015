for K in 32 64
do

for hmmKappa in 0 100
do

for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge
do

python LaunchRun.py \
  --dataName ToyARK13 \
  --obsModelName AutoRegGauss \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 4 \
  --launchCode $1 \
  --nickName may08

done
done
done
