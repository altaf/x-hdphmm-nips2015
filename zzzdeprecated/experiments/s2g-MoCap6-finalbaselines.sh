for K in 30 60
do

for hmmKappa in 20 #0 20 100
do

#for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge 
#for algName in bnpyHDPHMMstoch
for algName in foxHDPHMMsampler
do

python LaunchRun.py \
  --dataName MoCap6 \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 10 \
  --nickName finalbaselines \
  --nBatch 6 \
  $*
  
done
done
done
