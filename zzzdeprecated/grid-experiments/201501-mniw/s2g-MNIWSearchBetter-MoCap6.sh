## Discover best obsmodel parameters for MoCap6 dataset

dataName='MoCap6'
export jobName='MNIWSearch'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=short

export deleteStartLap=8
export VMat=same

for mergeStartLap in 5
do
echo "<<<<<<<<<<<<<<<<<<<<<<< mergeStartLap=$mergeStartLap"
export mergeStartLap=$mergeStartLap

for aModel in HDPHMM
do

for obsModel in AutoRegGauss
do

for alg in delmerge
do

for ECovMat in diagcovfirstdiff
do

for sF in 0.2 0.25  
do

for initname in 'truelabels' 'repeattruelabels'
do

  if [[ $initname == 'truelabels' ]]; then
    NTASK=1
    MAXTASK=1
  else
    NTASK=1-40
    MAXTASK=40
  fi

for nBatch in 1
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname -1 $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'runserial' ]]; then
    export nTask=$MAXTASK
    eval $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
done
done
done
done
done
exit
