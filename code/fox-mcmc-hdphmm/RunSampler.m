function RunSampler(dataName, ...
    K, alpha, gamma, hmmKappa, ...
    obsModelType, ECovMat, VMat, MMat, sF, kappa, ...
    initpath, taskoutpath, nLap, saveEvery, saveEveryScaleFactor, seqID)
%% Executable script for launching one run of the HDPHMM sampler
%
%% REQUIREMENTS
% ---------------------
% 1) x-hdphmm-nips2015 codebase.
% Set the install directory of this repo to env var XHMMROOT
% >>> $ export XHMMROOT=/path/to/x-hdphmm-nips2015/
% 2) Tom Minka's lightspeed toolbox.
% This is included with the repo: x-hdphmm-nips2015/code/lightspeed/
% To compile required mex files, do the following at a command line:
% >>> $ cd /path/to/x-hdphmm-nips2015/
% >>> $ make lightspeed
%
%% INPUT
% ---------------------
% dataName : string name of dataset, like 'DDToyHMM' or 'MoCap6'
%            This name prompts loading of dataset MAT file
%            which is located at $XHMMROOT/datasets/<dataName>/HMMdataset.mat
% K : int, num of initial components
% alpha : double, HDP second-level concentration parameter
% gamma : double, HDP root-level concentration parameter
% hmmKappa : double, HDP stickyness parameter
% obsModelType : string, one of 'AR' or 'Gauss'
% ECovMat : string, names procedure for setting expected cov mat under prior
% VMat : string, names procedure for setting prior parameter V (sets cov of A)
%        [AR only]
% MMat : string, names procedure for setting prior parameter M (mean of A)
%        [AR only]
% sF : double, 
% kappa : double, multiplier for scale of covariance on \mu
%         [Gauss only]
% initpath : string filesystem path to a directory
%            will load a MAT file called Lap0000.000MAPStateSeqs.mat
%            from this directory to initialize the sampler's state seq. vars Z
% taskoutpath : string filesystem path to a directory
%               where all results of this sampler run will be stored.
%               THIS DIRECTORY WILL BE ERASED AT THE START OF THE RUN,
%               so that no old information hangs around
% nLap : int, number of passes through full dataset to perform
% saveEvery : int, number of passes before we save to disk (see store_stats.m)
%
%% OUTPUT
% ---------------------
% None. Results are saved to disk at location specified by taskoutpath.

if exist('seqID', 'var') < 1
  seqID = -1;
end

fprintf('PATH\n%s\n\n', getenv('PATH'));
fprintf('PYTHONPATH\n%s\n\n', getenv('PYTHONPATH'));
fprintf('XHMMROOT\n%s\n\n', getenv('XHMMROOT'));
fprintf('LIGHTSPEEDROOT\n%s\n\n', getenv('LIGHTSPEEDROOT'));

ext = mexext();
hasLightspeed = exist(['randgamma.' ext], 'file') && randgamma(1);
if ~hasLightspeed
    error('Lightspeed install not complete.\n At command line, type "make lightspeed" in the root directory of the x-hdphmm-nips2015 repository.');
end

% Set Random Seed, based on the taskoutpath
% so that this run is reproduceable given same args next time.
[jobpath,taskid,~] = fileparts(taskoutpath);
[~, jobname, ~] = fileparts(jobpath);
loc = strfind(jobname, '-');
if ~isempty(loc)
   jobname = jobname(1:loc); 
end
pow26vec = 26.^((length(jobname)-1):-1:0); % [26^3 26^2 26^1 1]
jobnameNum = mod(sum(pow26vec .* jobname), 1e8);
if isempty(taskid)
   taskid = '1'; 
end
seed = jobnameNum + str2double(taskid);
fprintf('Random seed for initialization: %d\n', seed);
randomseed([seed 0 0]);
rng(seed);

%% Load Data
if ischar(dataName)
    data_struct = LoadHMMDatasetFromBNPYFormat(dataName, seqID);
else
    data_struct = dataName;
end
if ischar(K)
    K = str2double(K);
    alpha = str2double(alpha);
    gamma = str2double(gamma);
    hmmKappa = str2double(hmmKappa);
    sF = str2double(sF);
    kappa = str2double(kappa);
    nLap = str2double(nLap);
    saveEvery = str2double(saveEvery);
    saveEveryScaleFactor = str2double(saveEveryScaleFactor);
end

if strcmp(obsModelType, 'AutoRegGauss')
  obsModelType = 'AR';
elseif strcmp(obsModelType, 'Gauss')
  obsModelType = 'Gaussian';
end


settings = CreateSettings(K, taskoutpath, nLap, saveEvery, saveEveryScaleFactor);
% Create directory in which to save files if it does not currently exist:
if ~exist(settings.saveDir,'file')
    mkdir(settings.saveDir);
%else
    % COMMENTED OUT BY mhughes on 05/19
    % because we now rely on launcher script to discard old files
    %delete(fullfile(settings.saveDir, '*.mat'));
    %delete(fullfile(settings.saveDir, '*.txt'));
end

HMMmodel = CreateHMMAllocModel(settings, alpha, gamma, hmmKappa);
obsModel = CreateHMMObsModel(data_struct, obsModelType, ...
                             ECovMat, VMat, MMat, sF, kappa);
model = struct();
model.HMMmodel = HMMmodel;
model.obsModel = obsModel;


if ~isfield(settings,'saveMin')
    settings.saveMin = 1;
end
resample_kappa = settings.resample_kappa;
Kz = settings.Kz;
Niter = settings.Niter;

%%%%%%%%%%        Initialize variables             %%%%%%%%%%
%%%%%%%%%%                                         %%%%%%%%%%
fprintf('Initialization...\n');
% Set the starting iteration:
n_start = 1;

% Build initial structures for parameters and sufficient statistics:
[theta Ustats stateCounts hyperparams data_struct model S] = initializeStructs(model,data_struct,settings);

obsModel = model.obsModel;  % structure containing the observation model parameters
obsModelType = obsModel.type;   % type of emissions including Gaussian, multinomial, AR, and SLDS.
HMMhyperparams = model.HMMmodel.params; % hyperparameter structure for the HMM parameters
HMMmodelType = model.HMMmodel.type; % type of HMM including finite and HDP

% Resample concentration parameters:
hyperparams = sample_hyperparams_init(stateCounts,hyperparams,HMMhyperparams,HMMmodelType,resample_kappa);

% Load state sequence for initialization from disk.
[stateSeq INDS stateCounts] = loadInitialStateSeq(data_struct, initpath, Kz);

% Sample the transition distributions pi_z, initial distribution
% pi_init, emission weights pi_s, and global transition distribution beta
% (only if HDP-HMM) from the priors on these distributions:
stateCounts = sample_tables(stateCounts,hyperparams, 1.0/Kz * ones(1,Kz), Kz);
dist_struct = sample_dist(stateCounts, hyperparams, model);

Ustats = update_Ustats(data_struct,INDS,stateCounts,obsModelType);

% Sample emission params theta_{z,s}
theta = sample_theta(theta,Ustats,obsModel);

store_stats(S, 0, settings, stateSeq, dist_struct, theta, hyperparams);

%% Print initial covariance matrix
fprintf('Initial invSigma\n');
disp(squeeze(theta.invSigma(1, 1, :))');

settings.startTime = tic;
%%%%%%%%%% Run Sampler %%%%%%%%%%
for n=n_start:Niter
    
    % Sample z and s sequences given data, transition distributions,
    % HMM-state-specific mixture weights, and emission parameters:
    
    % Block sample (z_{1:T},s_{1:T})|y_{1:T}
    [stateSeq INDS stateCounts] = sample_zs(data_struct,dist_struct,theta,obsModelType);
    
    % Create sufficient statistics:
    Ustats = update_Ustats(data_struct,INDS,stateCounts,obsModelType);
    
    % Based on mode sequence assignment, sample how many tables in each
    % restaurant are serving each of the selected dishes. Also sample the
    % dish override variables:
    stateCounts = sample_tables(stateCounts,hyperparams,dist_struct.beta_vec,Kz);
    
    % Sample the transition distributions pi_z, initial distribution
    % pi_init, emission weights pi_s, and avg transition distribution beta:
    dist_struct = sample_dist(stateCounts,hyperparams,model);
    
    % Sample theta_{z,s}'s conditioned on the z and s sequences and the
    % sufficient statistics structure Ustats:
    theta = sample_theta(theta,Ustats,obsModel);
    
    % Resample concentration parameters:
    % DISABLED by mhughes.
    %hyperparams = sample_hyperparams(stateCounts,hyperparams,HMMhyperparams,HMMmodelType,resample_kappa);
    
    % Build and save stats structure:
    S = store_stats(S, n, settings, stateSeq, dist_struct,theta,hyperparams);
    
    if (rem(n, 10) == 0) || (n <= 4)
        FMT_MSG = '  %5d/%d after %5.1f sec | K %4d \n';
        fprintf(FMT_MSG, n, Niter, toc(settings.startTime), settings.Kz);
    end
end
if rem(n, 10) ~= 0
    fprintf(FMT_MSG, n, Niter, toc(settings.startTime), settings.Kz);
end
S = store_stats('final', n, settings, stateSeq, dist_struct,theta,hyperparams);
