for hmmKappa in 0 100
do

for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge
do

for K in 100
do
 
initname='randcontigblocks'

python LaunchRun.py \
  --dataName ChromatinCD4T \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 5 \
  --launchCode $1 \
  --nickName may19

done
done
done
