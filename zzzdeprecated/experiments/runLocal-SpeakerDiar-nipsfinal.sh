#for meetingNum in #1 3 16 21 # `seq 1 21`
for meetingNum in $* 
#2 4 5 6 7 8 9 10 11 12 13 14 15 17 18 19 20
do

echo "--------------------- meetNum $meetingNum"

for K in 1
do

for hmmKappa in 100
do

for creationProposalName in randBlocks bisectExistingBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --nickName nipsfinal \
  --dataName SpeakerDiar \
  --meetingNum $meetingNum \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 5 \
  --creationProposalName $creationProposalName \
  --minBlockSize 10 \
  --maxBlockSize 500 \
  --nLap 100 \
  --creationLapDelim_early 5 \
  --creationLapDelim_late 50 \
  --creationNumProposal_early 10 \
  --creationNumProposal_late 4 \
  --launchCode run

done
done
done
done
done
