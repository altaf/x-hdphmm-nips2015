global dataName
dataName = 'DDToyHMM'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 67]
  Kticks = [8, 25, 50, 75, 100]
  Laplims = [0, 3100]
  Lapticks = [1, 10, 100, 1000]
  Hlims = [-.025, 0.8]
  Hticks = [0, 0.2, 0.4, 0.6]
  ELBOlims = [-1.65, -1.4]
  ELBOticks = [-1.65, -1.6, -1.55, -1.5, -1.45]

  # Size of state seq plots
  ZW = 12 
  ZH = 3.5
  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks, 
                 ELBOlims, ELBOticks,
                 ZW, ZH)

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    C = [
        [166,206,227], # blue
        [31,120,180],
        [178,223,138], # green
        [51,160,44],
        [253,191,111], # orange
        [255,127,0],
        [202,178,214], # purple
        [106,61,154],
        ]
    C = np.asarray(C)/255.0;
    L = 100
    grayVals = np.linspace(0.15, 0.55, L)
    for ell in xrange(L):
        grayColor = np.asarray([grayVals[L-ell-1], 0, 0])
        #grayColor = grayVals[L-ell-1] * np.ones(3)
        C = np.vstack([C, grayColor[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap


def getColorMap():
  ColorMap = dict()
  ColorMap['memo'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('8', '-'), ('50','--'), ('100', '-')])
  return StyleMap

algVals = 'bnpyHDPHMMstoch,bnpyHDPHMMmemo,bnpyHDPHMMdelmerge'
suffix = '-lRateDelay=1-lRatePower=0.51'
def makeJobKeyPathMap(
        KVals='50,100',
        kappaVals='0,100',
        algVals=algVals):
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s-Sticky=%s-K=%s'
    path = "may11-alg=%s-lik=Gauss-hmmKappa=%s-ECovMat-eye-sF=1.0"
    path += "-K=%s-initname=randcontigblocks-nBatch=8"
    for alg in algVals.split(','):
        for kappa in kappaVals.split(','):
            for K in KVals.split(','):
                algkey = str(alg) # copy
                algkey = algkey.replace('bnpy', '')
                algkey = algkey.replace('HDPHMM', '')
                algkey = algkey.replace('fox', '')

                jobpath = path % (alg, kappa, K)
                jobkey = key % (algkey, kappa, K)

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

def getSubsetByName(JJ, name):
    JJ2 = OrderedDict()
    for key, path in JJ.items():
        if key.count(name):
            JJ2[key] = path
    return JJ2
