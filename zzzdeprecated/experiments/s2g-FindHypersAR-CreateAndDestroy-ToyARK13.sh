dataName='ToyARK13'
D=3
ECovMat='eye'
VMat='same'
MMat='zero'


for hmmKappa in 0 100
do

for nu in 5
do

for sF in 0.05 0.1 0.2 0.4 0.8
do
sV=$sF

for init in truelabels repeattruelabels 
do

  if [[ $init == 'truelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'repeattruelabels' ]]; then
     K=-1
     initname=$init
  else
     K=$init
     initname='randcontigblocks'
  fi

python LaunchRun.py \
  --launchCode $1 \
  --queue short \
  --dataName $dataName \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName  bnpyHDPHMMcreateanddestroy \
  --doFullPassBeforeMstep 1 \
  --nTask 4 \
  --nLap 40 \
  --minLaps 20 \
  --nu $nu \
  --ECovMat $ECovMat \
  --sF $sF \
  --VMat $VMat \
  --sV $sV \
  --MMat $MMat \
  --nDocTotal 26 \
  --T 500 \
  --nBatch 2 \
  --jobname_final "findhypers-hmmKappa=$hmmKappa-nu=$nu-ECovMat=$ECovMat-sF=$sF-VMat=$VMat-MMat=$MMat-initname=$initname"

done
done
done
done
