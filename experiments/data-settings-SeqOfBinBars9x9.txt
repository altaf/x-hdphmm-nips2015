N 50
maxT 10000
Ktrue 20

BNPYDATADIR $XHMMROOT/datasets/SeqOfBinBars9x9/batches/
dataPath $XHMMROOT/datasets/SeqOfBinBars9x9/batches/

nBatch 25
nLap 200
nLap_test 5

saveEarly 10
saveEvery 10
saveEveryLogScaleFactor 2

obsModelName Bern
lam1 0.1
lam0 0.3

initBlockLen 100 

doFullPassBeforeMstep 0

dtargetMaxSize 10
