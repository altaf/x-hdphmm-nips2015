## Discover best obsmodel parameters for MoCap6 dataset

dataName='MoCap6'
export jobName='Search'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=short

export mergeStartLap=5
export deleteStartLap=8
export VMat=same
aModel=HDPHMM
obsModel=AutoRegGauss
alg=delmerge
ECovMat=diagcovfirstdiff
nBatch=1
sF=0.5
K=50

for MMat in eye #zero eye
do
  export MMat=$MMat

for hmmKappa in 100 #0 100
do
  export hmmKappa=$hmmKappa

for initname in 'randexamples' #'repeattruelabels'
do

  if [[ $initname == 'truelabels' ]]; then
    NTASK=1
    MAXTASK=1
  elif [[ $initname == 'repeattruelabels' ]]; then
    NTASK=1-11
    MAXTASK=31
    K=-1
  else
    NTASK=1-11
    MAXTASK=31
  fi

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'runserial' ]]; then
    export nTask=$MAXTASK
    eval $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
exit
