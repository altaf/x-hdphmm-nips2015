for K in 100 200
do

for hmmKappa in 100
do

for algName in bnpyHDPHMMdelmerge bnpyHDPHMMstoch bnpyHDPHMMmemo
do

python LaunchRun.py \
  --dataName MoCap124 \
  --doFullPassBeforeMstep 50 \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 5 \
  --nickName finalbaselines \
  --nBatch 20 \
  $*
  
done
done
done
