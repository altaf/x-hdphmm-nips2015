## Script for launching jobs on the grid.
# Usage
# --------------
# s2g____.sh <no args>
#    print out list of jobs that will be run
# s2g____.sh launch
#    send all jobs to the grid

dataName='Chr7'
export jobName='Wed'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=vlong
NTASK=1-2

export aModel=HDPHMM
export obsModel=DiagGauss
export ECovMat=covdata
export sF=0.75
export nBatch=20
export learnRate=aa
export initBlockLen=100
export deleteStartLap=2
export mergeStartLap=3 # Delaying merge to allow initialization more time??
export deleteFailLimit=15
export MMat=data


for hmmKappa in 0 300
do
  echo "======================== hmmKappa $hmmKappa"
  export hmmKappa=$hmmKappa

# Remember: Need to run with fixed before sampler
#for alg in sampler
for alg in delmerge stoch fixed
#for alg in delmerge
do

for initname in kmeans 
do

for K in 50
#for K in 50 100
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l vf=4G -l $QTYPE -q "*@@liv" $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
done
exit
