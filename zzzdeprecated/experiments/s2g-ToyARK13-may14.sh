for K in 50 100
do

for hmmKappa in 100
do

for algName in bnpyHDPHMMmerge #bnpyHDPHMMdelmerge bnpyHDPHMMmergepar #foxHDPHMMsampler #bnpyHDPHMMmemo bnpyHDPHMMdelmerge
do

python LaunchRun.py \
  --dataName ToyARK13 \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 4 \
  --nWorkers 4 \
  --launchCode $1 \
  --nickName may14 

done
done
done
