function settings = CreateSettings(Kz, saveDir, nLap, saveEvery, saveEveryLogScaleFactor)

settings.Kz = Kz;   % truncation level for mode transition distributions
settings.Ks = 1;  % truncation level for mode transition distributions

settings.Niter = nLap;  % Number of iterations of the Gibbs sampler
settings.resample_kappa = 0;  % Whether or not to use sticky model
settings.seqSampleEvery = 100; % How often to run sequential z sampling

settings.saveEveryLogScaleFactor = saveEveryLogScaleFactor;
settings.saveEvery = saveEvery;  % How often to save snapshots to disk
settings.storeEvery = 0;
settings.storeStateSeqEvery = 0;
settings.ploton = 0;  % Whether or not to plot the mode sequence while running sampler
settings.plotEvery = nLap * 100;
settings.plotpause = 0;  % Length of time to pause on the plot
settings.saveDir = saveDir;  % Directory to which to save files

