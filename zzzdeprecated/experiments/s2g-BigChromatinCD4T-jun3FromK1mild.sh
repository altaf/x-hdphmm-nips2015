hmmKappa=100
initname='randcontigblocks'

for creationProposalName in bisectGrownBlocks #randBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

for K in 1 
do

python LaunchRun.py \
  --doFullPassBeforeMstep 1 \
  --dataName BigChromatinCD4T \
  --K $K \
  --creationProposalName $creationProposalName \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 5 \
  --nickName jun3mild \
  --creationLapDelim_early 2 \
  --creationLapDelim_late 20 \
  --creationNumProposal_early 1 \
  --creationNumProposal_late 1 \
  --creationKfresh_early 2 \
  --creationKfresh_late 2 \
  --Kmax 70 \
  --nRefineIters 1 \
  --mergePerLap 10 \
  --dtargetMaxSize 10 \
  --deleteStartLap 2 \
  --mergeStartLap 2 \
  --nGlobalIters 1 \
  --nGlobalItersBigChange 2 \
  $*
done
done
done



















