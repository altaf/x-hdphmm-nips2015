function obsModel = CreateHMMObsModel(data_struct, obsModelType, ...
                                       ECovMat, VMat, MMat, sF, kappa)

d = data_struct(1).d;

obsModel.type = obsModelType;
if strcmp(obsModelType,'AR')
    % Order of AR process:
    obsModel.r = 1;
    m = d;
    priorType = 'MNIW';
else
    m = d;
    priorType = 'NIW';
end

if ischar(ECovMat)
    if strcmp(ECovMat, 'eye')
        ECovMat = eye(d);     
    elseif strcmp(ECovMat, 'covdata')
        Xall = [data_struct(:).obs];
        ECovMat = cov(Xall', 1);
    elseif strcmp(ECovMat, 'diagcovdata')
        Xall = [data_struct(:).obs];
        ECovMat = diag(diag(cov(Xall', 1)));
    elseif strcmp(ECovMat, 'diagcovfirstdiff')
        Xdiff = [data_struct(:).obs] - [data_struct(:).X];
        ECovMat = cov(Xdiff', 1);
        ECovMat = diag(diag(ECovMat));
    else
        error('ERROR: Unrecognized value for ECovMat');
    end
end
if ischar(VMat)
    if strcmp(VMat, 'eye')
        VMat = sF * eye(d);     
    elseif strcmp(VMat, 'same')
        VMat = sF * ECovMat;
    else
        error('ERROR: Unrecognized value for VMat');
    end
end
if ischar(MMat)
     if strcmp(MMat, 'eye')
        MMat = eye(d);
     elseif strcmp(MMat, 'zero')
        MMat = zeros(d,d);
     else
        error('ERROR: Unrecognized value for MMat');
     end
end

% Degrees of freedom nu
obsModel.params.nu = d + 2;

% Scale matrix (aka B)
obsModel.params.nu_delta = (obsModel.params.nu-d-1) * sF * ECovMat;

fprintf('ECovMat=\n');
disp(sF * ECovMat(1:2, 1:2));

% Type of prior on dynamic parameters. Choices include matrix normal
% inverse Wishart on (A,Sigma) and normal on mu ('MNIW-N'), matrix normal
% inverse Wishart on (A,Sigma) with mean forced to 0 ('MNIW'), normal on A,
% inverse Wishart on Sigma, and normal on mu ('N-IW-N'), and fixed A,
% inverse Wishart on Sigma, and normal on mu ('Afixed-IW-N').  NOTE: right
% now, the 'N-IW-N' option is only coded for shared A!!!
obsModel.priorType = priorType;

switch obsModel.priorType
    case 'NIW'
        obsModel.params.M  = zeros(d, 1); % col vector of length d
        obsModel.params.K =  kappa;
        
    case 'MNIW'
        % Mean and covariance for A matrix:
        obsModel.params.M  = MMat; 

        % Inverse covariance along rows of A (sampled Sigma acts as
        % covariance along columns):
        obsModel.params.K =  VMat;
        
end

% Always using DP mixtures emissions, with single Gaussian forced by
% Ks=1...Need to fix.
obsModel.mixtureType = 'infinite';
