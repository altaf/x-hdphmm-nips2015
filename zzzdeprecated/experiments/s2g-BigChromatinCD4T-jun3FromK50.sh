hmmKappa=100
initname='randcontigblocks'

for creationProposalName in bisectGrownBlocks #randBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

for K in 50 
do

python LaunchRun.py \
  --doFullPassBeforeMstep 20 \
  --dataName BigChromatinCD4T \
  --K $K \
  --creationProposalName $creationProposalName \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 1 \
  --nickName jun3 \
  --creationLapDelim_early 2 \
  --creationLapDelim_late 20 \
  --creationNumProposal_early 2 \
  --creationNumProposal_late 2 \
  --creationKfresh_early 2 \
  --creationKfresh_late 2 \
  --Kmax 100 \
  --nRefineIters 1 \
  --mergePerLap 10 \
  --dtargetMaxSize 10 \
  --deleteStartLap 2 \
  --mergeStartLap 2 \
  --nGlobalIters 1 \
  --nGlobalItersBigChange 3 \
  $*
done
done
done



















