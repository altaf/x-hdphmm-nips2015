global dataName
dataName = 'SpeakerDiar'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

def setUp():
  Klims = [-0.5, 27]
  Kticks = [0, 5, 10, 15, 20, 25]

  Laplims = [0, 2100]
  Lapticks = [1, 10, 100, 1000]

  Hlims = [-0.03, 0.85]
  Hticks = [0, 0.2, 0.4, 0.6, 0.8]

  ELBOlims = None #[-1.65, -1.4]
  ELBOticks = None #[-1.6, -1.55, -1.5, -1.45]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5
  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  return makeKeyPathMap()

BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet[2:3] + GreenSet[2:3] + OrangeSet[2:3] + PurpleSet[2:3] \
        + BlueSet[1:2] + GreenSet[1:2] + OrangeSet[1:2] + PurpleSet[1:2]

    C = np.asarray(Colors)/255.0;
    L = 10
    shadeVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([shadeVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )

    # Add two gray states for bg state values -2 and -1
    C = np.vstack([
        0.3 * np.ones((1,3)), 
        0.6 * np.ones((1,3)), 
        C])

    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['delmerge'] = 'r'
    ColorMap['randBlocks'] = 'c'
    ColorMap['bisect'] = 'm'
    ColorMap['birth'] = 'm'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=25'] = '-'
    StyleMap['K=1'] = '-'
    return StyleMap

kappaVals = ['100']
KVals = ['25']

algNameVals = [
  'foxHDPHMMsampler',
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
suffix = '-lRateDelay=1-lRatePower=0.51'

def makeKeyPathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'K=%s %s Sticky=%s'
    path = "finalbaselines-alg=%s-lik=Gauss-hmmKappa=%s"
    path += "-ECovMat-diagcovdata-sF=0.5"
    path += "-K=%s-initname=randcontigblocks-nBatch=1"
    for algName in algNameVals:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (K, algName, kappa)

                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
creationProposalNameVals = [
  'randBlocks',
  'bisectExistingBlocks',
  ]
# nipsfinal-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-K=1-initname=randcontigblocks-nBatch=1-creationProposalName=randBlocks-lik=Gauss-ECovMat=diagcovdata-sF=0.5/
def makeKeyPathMap_ForBirths():
    J = OrderedDict()
    key = 'K=1 %s'
    path = "nipsfinal-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "K=1-initname=randcontigblocks-nBatch=1"
    path += "-creationProposalName=%s"
    path += "-lik=Gauss-ECovMat=diagcovdata-sF=0.5"
    for cpName in creationProposalNameVals:
                jobpath = path % (cpName)
                jobkey = key % (cpName.replace('Existing', ''))
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    


def makeKeyPathMap_ForLonger():
    J = OrderedDict()
    key = 'K=25 %s'
    path = "longerfinal-alg=%s-lik=Gauss-hmmKappa=100"
    path += "-ECovMat-diagcovdata-sF=0.5-K=25-initname=randcontigblocks-nBatch=1"
    for algName in algNameVals:
        jobkey = key % (algName)
        jobpath = path % (algName)
        J[jobkey] = jobpath 

    bpath = "longerfinal-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-K=25"
    bpath += "-initname=randcontigblocks-nBatch=1"
    bpath += "-creationProposalName=bisectGrownBlocks"
    bpath += "-lik=Gauss-ECovMat=diagcovdata-sF=0.5"
    J["K=25 birth"] = bpath
    return J

def isValidJobKeyAndPath(jobkey, jobpath, n='1'):
    if not os.path.exists(PlotUtil.MakePath(jobpath, n=n)):
        return False
    return True

def plotScatterComparison(jpath1, jpath2, nTask=10):
    scores = dict()
    PlotUtil.pylab.hold('on');
    xs = np.linspace(-1, 1, 10);
    PlotUtil.pylab.plot(xs, xs, 'k--');
    for n in range(21):
        path1 = PlotUtil.MakePath(jpath1, n=str(n+1))
        path2 = PlotUtil.MakePath(jpath2, n=str(n+1))
        scores[n] = np.zeros(nTask)
        for task in range(nTask):
            hpath1 = os.path.join(path1, str(task+1), 'hamming-distance.txt')
            dist1 = np.loadtxt(hpath1)[-1]
            hpath2 = os.path.join(path2, str(task+1), 'hamming-distance.txt')
            dist2 = np.loadtxt(hpath2)[-1]
            PlotUtil.pylab.plot(dist1, dist2, 'k.', markersize=12)
            scores[n][task] = dist1 - dist2
    PlotUtil.pylab.axis('image');
    return scores
