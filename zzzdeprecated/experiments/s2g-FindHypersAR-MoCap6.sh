dataName='MoCap6'
D=12
ECovMat='diagcovfirstdiff'
VMat='same'
MMat='eye'

hmmKappa=300

for nu in 14
do

for algName in bnpyHDPHMMdelmerge bnpyHDPHMMcreateanddestroy
do

for sF in 0.8 1.0 1.2
do
sV=$sF

for init in truelabels subdividetruelabels 30 #repeattruelabels truelabels #repeattruelabels #25' 
do

  if [[ $init == 'truelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'subdividetruelabels' ]]; then
     K=-1
     initname=$init
  else
     K=$init
     initname='randcontigblocks'
  fi

python LaunchRun.py \
  --launchCode $1 \
  --dataName $dataName \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --doFullPassBeforeMstep 1 \
  --nTask 4 \
  --nLap 20 \
  --minLaps 20 \
  --nu $nu \
  --ECovMat $ECovMat \
  --sF $sF \
  --VMat $VMat \
  --sV $sV \
  --MMat $MMat \
  --queue short \
  --jobname_final "findhyp2-alg=$algName-hmmKappa=$hmmKappa-nu=$nu-ECovMat=$ECovMat-sF=$sF-VMat=$VMat-MMat=$MMat-initname=$initname"

done
done
done
done
