global dataName
dataName = 'DDToyHMM'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

# Define colormap for visualizing true states
BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

# Define value ranges used for experiments
kappaVals = ['0', '50']
KVals = ['50', '100']
KVals_Birth = ['1', '10', '50']
algNameVals = [
  'bnpyHDPHMMstoch',
  'foxHDPHMMsampler',
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
suffix = '-lRateDelay=1-lRatePower=0.51'


def setUp(lineStyleByKey='K'):
  Klims = [-0.5, 105]
  Kticks = [8, 20, 40, 60, 80]

  Laplims = [0, 5000]
  Lapticks = [1, 10, 100, 1000]

  Hlims = [-0.03, 0.85]
  Hticks = [0, 0.2, 0.4, 0.6, 0.8]

  ELBOlims = [-1.65, -1.395]
  ELBOticks = [-1.6, -1.5, -1.4]

  # Size of state seq plots
  ZW = 8
  ZH = 4.5 # Dont shrink this. Text gets cut off!

  if lineStyleByKey.count('K'):
      StyleMap = getLineStyleMap_ByKValue()
  else:
      StyleMap = getLineStyleMap_ByStickyValue()

  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), StyleMap, 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  J = makePathMap()
  J.update(makePathMap_ForBirths())
  return J


def getStateSeqColorMap(nErrorShades=4):
    ''' Create colormap with entry for each of the 8 true states.

    Extraneous state indices are assigned to different shades of red.

    Returns
    -------
    Cmap : ListedColormap
    '''
    from matplotlib.colors import ListedColormap
    Colors = BlueSet[1:3] + GreenSet[1:3] + OrangeSet[1:3] + PurpleSet[1:3]
    C = np.asarray(Colors)/255.0;
    grayVals = np.linspace(0.15, 0.85, nErrorShades)
    for ell in xrange(nErrorShades):
        shadeOfRed = np.asarray([grayVals[nErrorShades-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['delmerge'] = 'r'
    ColorMap['birth'] = 'm'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap_ByKValue():
    StyleMap = OrderedDict()
    StyleMap['K=50'] = '--'
    StyleMap['K=100'] = '-'
    StyleMap['birth'] = '-'
    return StyleMap

def getLineStyleMap_ByStickyValue():
    StyleMap = OrderedDict()
    StyleMap['Sticky=0'] = '--'
    StyleMap['Sticky=50'] = '-'
    return StyleMap


def makePathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "K=50 Sticky=50 alg=memo"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s Sticky=%s K=%s'
    path = "nipsexperiment-alg=%s-lik=Gauss-hmmKappa=%s"
    path += "-ECovMat-eye-sF=1.0"
    path += "-K=%s-initname=randcontigblocks-nBatch=8"
    for algName in algNameVals:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (algName, kappa, K)
                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp.replace('nBatch=6', 'nBatch=1')
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makePathMap_ForBirths():
    J = OrderedDict()
    key = 'birth Sticky=%s K=%s'
    path = "nipsexperiment-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=%s-"
    path += "K=%s-initname=randcontigblocks-nBatch=8"
    path += "-creationProposalName=bisectExistingBlocks"
    path += "-lik=Gauss-ECovMat=eye-sF=1.0"
    for kappa in kappaVals:
        for K in KVals_Birth:
                jobpath = path % (kappa, K)
                jobkey = key % (kappa, K)
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makeLegendInOwnFigure(
        Jdict,
        names = ['stoch', 'sampler', 'memo',
                 'delete,merge', 'birth,delete,merge',
                 'Sticky=0', 'Sticky=50'],
    ):
    ''' Create legend for trace plots, in separate matplotlib figure.
    '''
    # Split up each name into original name (key in Jdict) and legend name
    origNames = list()
    legendNames = list()
    for name in names:
        if name.count(":") > 0:
            fields = name.split(":")
            origNames.append(fields[0])
            legendNames.append(fields[1])
        else:
            origNames.append(name)
            legendNames.append(name)
    # Build job dict with one entry for each of the specified names
    J2 = OrderedDict()
    for name in origNames:
        Jmatch = getSubsetByName(Jdict, name, keepName=1)
        if len(Jmatch.keys()) == 0:
            raise ValueError("Cannot find key %s in provided job path dict" % (
                name))
        firstkey = Jmatch.keys()[0]
        Jsingle = dict()
        Jsingle[name + firstkey] = Jmatch[firstkey]
        J2.update(Jsingle)
    # Make trace plot with these jobs only
    PlotUtil.plotHammingDist(J2, loc=None, xscale='log')
    # Grab current axes of this trace plot
    axH = PlotUtil.pylab.gca();
    # Set all line handles to wide, solid lines
    lineHandles, lineLabels = axH.get_legend_handles_labels()
    for ii in range(len(lineHandles)):
        lineHandles[ii].set_linewidth(6)
        lineHandles[ii].set_linestyle('-')
        if names[ii].count('Sticky'):
            lineHandles[ii].set_color('k')
            lineHandles[ii].set_linewidth(3)
        if names[ii].count('Sticky=0'):
            lineHandles[ii].set_linestyle('--')
    # Remove lines from the plot entirely, so its just the legend
    axH.axis('off')
    for ii in range(len(axH.lines)):
        axH.lines.pop()
    # Show the legend
    axH.legend(lineHandles, legendNames)

def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

