function data_struct = LoadHMMDatasetFromBNPYFormat(dataName, seqID)

SpeakerDiarFiles = {
    'AMI_20041210-1052.mat',
    'AMI_20050204-1206.mat',
    'CMU_20050228-1615.mat',
    'CMU_20050301-1415.mat',
    'CMU_20050912-0900.mat',
    'CMU_20050914-0900.mat',
    'EDI_20050216-1051.mat',
    'EDI_20050218-0900.mat',
    'ICSI_20000807-1000.mat',
    'ICSI_20010208-1430.mat',
    'LDC_20011116-1400.mat',
    'LDC_20011116-1500.mat',
    'NIST_20030623-1409.mat',
    'NIST_20030925-1517.mat',
    'NIST_20051024-0930.mat',
    'NIST_20051102-1323.mat',
    'TNO_20041103-1130.mat',
    'VT_20050304-1300.mat',
    'VT_20050318-1430.mat',
    'VT_20050623-1400.mat',
    'VT_20051027-1400.mat'};

if strcmp(dataName, 'SpeakerDiar')
    seqID = str2double(seqID);
    assert(seqID > 0);
    assert(seqID <= 21);
    matfilepath = fullfile(getenv('BNPYROOT'), ...
        'datasets', 'rawData', 'speakerDiarizationData', ...
        SpeakerDiarFiles{seqID});
else
    matfilepath = fullfile(getenv('XHMMROOT'), ...
        'datasets', dataName, 'HMMdataset.mat');
end

if exist(matfilepath, 'file')
    SaveVars = load(matfilepath);
else
    disp(matfilepath);
    error(['CANNOT FIND DATASET ' dataName]);
end

nSeq =  length(SaveVars.doc_range) - 1;
data_struct = repmat(struct('obs', [], 'd', []), nSeq, 1);

maxT = 0;
for n = 1:nSeq
   start = SaveVars.doc_range(n)+1;
   stop = SaveVars.doc_range(n+1);
   data_struct(n).d = size(SaveVars.X, 2); 
   data_struct(n).obs = SaveVars.X(start:stop, :)';
   if isfield(SaveVars, 'Xprev')
      data_struct(n).X = SaveVars.Xprev(start:stop, :)'; 
   end
   T = size(data_struct(n).obs, 2);
   maxT = max(T, maxT);
end
data_struct(1).test_cases = [1:length(data_struct)];

fprintf('Loaded dataset. N=%d sequences. maxT=%d.\n', ...
    length(data_struct), ...
    maxT);
