#!/usr/bin/python

import subprocess

CMD = """
python LaunchRun.py \
 --dataName DDToyHMM \
 --K $K \
 --hmmKappa $hmmKappa \
 --algName $algName \
 --nickName nipsexperiment \
 --nTask 10 \
 --creationProposalName bisectExistingBlocks \
 --creationLapDelim_early 2 \
 --creationLapDelim_late 20 \
 --creationNumProposal_early 5 \
 --creationNumProposal_late 1 \
 --nRefineIters 1 \
 --nGlobalIters 1 \
 --nGlobalItersBigChange 5 \
"""

for hmmKappa in [0, 50]:
    for K in [1, 10, 50]:
        for algName in ['bnpyHDPHMMcreateanddestroy']:

            # Execute the command
            curCMD = CMD + ""
            curCMD = curCMD.replace('$K', str(K))
            curCMD = curCMD.replace("$algName", algName)
            curCMD = curCMD.replace("$hmmKappa", str(hmmKappa))
            proc = subprocess.Popen(curCMD.split(), shell=False)
            proc.wait()

