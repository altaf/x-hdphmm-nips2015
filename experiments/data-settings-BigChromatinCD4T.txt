N 173
maxT 160000

BNPYDATADIR /data/liv/biodatasets/CD4TCellLine/wholegenomebatches/
dataPath /data/liv/biodatasets/CD4TCellLine/wholegenomebatches/

nBatch 173
nLap 100
nLap_test 5

saveEarly 10
saveEvery 0.11560693641
saveEveryLogScaleFactor 0

printEvery 0.11560693641
traceEvery 0.11560693641

obsModelName Bern
lam1 0.1
lam0 0.3

doFullPassBeforeMstep 0

customFuncPath None
dtargetMaxSize 10

initBlockLen 50

mergePerLap 10

Kmax 70
creationProposalName bisectGrownBlocks
creationNumProposal_early 1
creationNumProposal_late 1
creationLapDelim_early 2
creationLapDelim_late 20
creationKfresh_early 2
creationKfresh_late 2

minBlockSize 20
maxBlockSize 500
growthBlockSize 25
nRefineIters 1
nGlobalIters 1
nGlobalItersBigChange 5


















