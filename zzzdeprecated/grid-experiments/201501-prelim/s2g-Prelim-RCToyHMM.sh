## Script for launching jobs on the grid.
# Usage
# --------------
# s2g____.sh <no args>
#    print out list of jobs that will be run
# s2g____.sh launch
#    send all jobs to the grid

dataName='RCToyHMM'
export jobName='Prelim'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=short
NTASK=1-10

export aModel=HDPHMM
export obsModel=Gauss
export ECovMat=eye
export sF=1
export nBatch=1
export initname=randexamplesbydist

# Remember: Need to run with fixed before sampler
for alg in fixed #sampler #merge delmerge
do

for K in 16 32 64
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
exit
