for meetingNum in 11 21 1 16 # `seq 1 21`
do

echo "--------------------- meetNum $meetingNum"

for K in 25
do

for hmmKappa in 0 100
do

#for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge
#for algName in foxHDPHMMsampler
do

python LaunchRun.py \
  --dataName SpeakerDiar \
  --meetingNum $meetingNum \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 10 \
  --nickName finalbaselines \
  $*

done
done
done
done
