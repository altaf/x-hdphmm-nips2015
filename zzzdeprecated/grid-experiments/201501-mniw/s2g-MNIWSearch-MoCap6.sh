## Discover best obsmodel parameters for MoCap6 dataset

dataName='MoCap6'
export jobName='MNIWSearch'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=short

for mergeStartLap in 2 50
do
echo "<<<<<<<<<<<<<<<<<<<<<<< mergeStartLap=$mergeStartLap"
export mergeStartLap=$mergeStartLap

for aModel in HDPHMM
do

for obsModel in AutoRegGauss
do

for alg in delmerge
do

for ECovMat in covfirstdiff
do

for sF in 0.25 0.5 0.75 1
do

for initname in 'truelabels' 'repeattruelabels'
do

  if [[ $initname == 'truelabels' ]]; then
    NTASK=1
  else
    NTASK=1-25
  fi

for nBatch in 1
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname -1 $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
done
done
done
done
done
exit
