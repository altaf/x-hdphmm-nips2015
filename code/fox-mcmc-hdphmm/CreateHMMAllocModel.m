function HMMmodel = CreateHMMAllocModel(settings, alpha, gamma, hmmKappa)

% HDP-HMM hyperparameter settings:

% Trans prob hyperparameter
% Set so E[alpha] = alpha
HMMmodel.params.a_alpha=alpha;
HMMmodel.params.b_alpha=1;

% Top level hyperparameter
HMMmodel.params.a_gamma=gamma;
HMMmodel.params.b_gamma=1;
if settings.Ks>1
    HMMmodel.params.a_sigma = 1;
    HMMmodel.params.b_sigma = 0.01;
end

% Sticky bias towars self transition
HMMmodel.params.c=hmmKappa;
HMMmodel.params.d=1;
HMMmodel.type = 'HDP';
