#!/contrib/projects/EnthoughtPython/epd64/bin/python
#$ -cwd
#$ -S /bin/bash
#$ -o /data/liv/xdump/logs/$JOB_ID.$TASK_ID.out
#$ -e /data/liv/xdump/logs/$JOB_ID.$TASK_ID.err
#$ -v PATH -v HOME -v SHELL -v BNPYROOT -v PYTHONPATH -v BNPYDATADIR

import argparse
import os
import subprocess
import sys

def run(**kwargs):
    ''' Execute single run of MCMC sampling for HDP-HMM 
    '''
    try:
        taskIDstr = os.getenv('SGE_TASK_ID')
    except TypeError, ValueError:
        taskIDstr = None
    if taskIDstr is None:
        taskIDstr = str(kwargs['taskID'])
    if 'meetingNum' in kwargs and int(kwargs['meetingNum']) > 0:
        dataName = kwargs['dataName'] + kwargs['meetingNum']
    else:
        dataName = kwargs['dataName']

    kwargs['outputdir'] = os.path.join(
        os.environ['BNPYOUTDIR'], dataName, 
        kwargs['jobname'], taskIDstr)
    createEmptyOutputDir(**kwargs)

    kwargs['initpath'] = kwargs['outputdir'].replace(
        'foxHDPHMMsampler', 'bnpyHDPHMMmemo')
    kwargs['initpath'] = kwargs['initpath'].replace('debug-', '')

    SetupArgs = \
        "addpath(genpath('%s')); " % (os.environ['LIGHTSPEEDROOT']) + \
        "addpath(genpath('%s')); " % (os.environ['FOXMCMCROOT'])

    MainArgs = \
        "RunSampler('$dataName', '$K', '$alpha', '$gamma', '$hmmKappa', " + \
        "'$obsModelName', '$ECovMat', '$VMat', '$MMat', '$sF', '$kappa', " + \
        "'$initpath', '$outputdir', '$nLap', '$saveEvery', '$saveEveryLogScaleFactor', '$meetingNum');"
        
    for key, val in kwargs.items():
        MainArgs = MainArgs.replace('$' + key, val)

    MATLABEXE = '/local/projects/matlab/current/bin/matlab'
    if not os.path.exists(MATLABEXE):
        MATLABEXE = 'matlab'

    rCMD = '-r "try, %s; %s; catch e, disp(e.message); end, exit;"' \
        % (SetupArgs, MainArgs)

    CMDlist = [MATLABEXE, '-nosplash', '-nodisplay', '-nodesktop']
    CMDlist.append(rCMD)

    print SetupArgs
    print MainArgs

    print ''
    print ''
    print ' '.join(CMDlist)
    print ''

    if 'JOB_ID' in os.environ:
        print 'Making GridInfo.txt...'
        with open(os.path.join(kwargs['outputdir'], 'GridInfo.txt'), 'w') as f:
            f.write(os.getenv('JOB_ID') + "\n")
            f.write('stdout: ' + os.getenv('SGE_STDOUT_PATH') + "\n")
            f.write('stderr: ' + os.getenv('SGE_STDERR_PATH') + "\n")
        # Create symlinks to captured stdout, stdout in output directory
        os.symlink(os.getenv('SGE_STDOUT_PATH'),
                   os.path.join(kwargs['outputdir'], 'stdout'))
        os.symlink(os.getenv('SGE_STDERR_PATH'),
                   os.path.join(kwargs['outputdir'], 'stderr'))

    with open(getLogfilePath(**kwargs), 'w') as logfile:
        logfile.write('Executing command: \n')
        
        proc = subprocess.Popen(
            CMDlist,
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
        for line in iter(proc.stdout.readline, b''):
            sys.stdout.write(line)
            logfile.write(line)
            logfile.flush()
        proc.wait()

        logfile.write('\n')
        logfile.flush()
    print '[TRAINING DONE].'
    print ' Output saved to file: %s' % (kwargs['outputdir'])
    print ' Transcript saved to file %s/transcript.txt' % (kwargs['outputdir'])


def getLogfilePath(**kwargs):
    ''' Create full path to file in outputdir where logs will be stored.
    '''
    return os.path.join(kwargs['outputdir'], 'transcript.txt')

def createEmptyOutputDir(outputdir='/tmp/', **kwargs):
    ''' Create specified path on the file system with empty contents.

    Postcondition
    -------------
    The directory outputdir will exist, with no content.
    Any required parent paths will be automatically created.
    Any pre-existing content will be deleted, to avoid confusion.
    '''
    from distutils.dir_util import mkpath
    # Ensure the path (including all parent paths) exists
    mkpath(outputdir)
    # Ensure the path has no content from previous runs
    for the_file in os.listdir(outputdir):
        file_path = os.path.join(outputdir, the_file)
        if os.path.isfile(file_path) or os.path.islink(file_path):
            os.unlink(file_path)


def arglist_to_dict(argList):
    ''' Transform list of name/val pairs into keyword dict.
    '''
    L = len(argList)
    kwargs = dict()
    for pos in xrange(0, L, 2):
      name = argList[pos][2:]
      val = argList[pos+1]
      kwargs[name] = val
    return kwargs

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    args, UNKlist = parser.parse_known_args()
    kwargs = dict(**args.__dict__)
    kwargs.update(arglist_to_dict(UNKlist))
    run(**kwargs)
