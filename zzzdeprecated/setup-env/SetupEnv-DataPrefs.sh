BNPYDATADIR="$XHMMROOT/datasets/$dataName/"
if [[ -d $BNPYDATADIR ]]; then
  export BNPYDATADIR=$BNPYDATADIR;
else
  export BNPYDATADIR='';
fi

if [[ $dataName == "MoCap6" ]]; then
  export N=6
  export Ktrue=12
  export maxT=380
  nLapLong=1000
  nLapShort=10
  saveEvery=20
elif [[ $dataName == "MoCap124" ]]; then
  export N=124
  export maxT=500
  nLapLong=1000
  nLapShort=1
  saveEvery=25
elif [[ $dataName == "Chr7" ]]; then
  export dataPath=/data/liv/biodatasets/K562CellLine/batches-chr7/
  export N=20cd
  export nLapLong=1000
  export nLapShort=1
  export saveEvery=1
  export dtargetMaxSize=10
elif [[ $dataName == "ToyARK13" ]]; then
  export N=52
  export Ktrue=13
  export maxT=800
  export nLapLong=2000
  export nLapShort=40
  export saveEvery=25
elif [[ $dataName == "DDToyHMM" ]]; then
  export N=32
  export Ktrue=8
  export maxT=1000
  nLapLong=2000
  nLapShort=5
  export saveEvery=25
elif [[ $dataName == "RCToyHMM" ]]; then
  export N=32
  export Ktrue=8
  export maxT=1000
  nLapLong=400
  nLapShort=5
elif [[ $dataName == "RCOToyHMM" ]]; then
  export N=32
  export Ktrue=8
  export maxT=200
  nLapLong=400
  nLapShort=5
else
  echo "ERROR: Unrecognized dataset: $dataName"
  exit
fi

if [[ -z $dataPath ]]; then
  export dataPath=$dataName
fi

export nLap=$nLapShort
if [[ -n $runCode ]]; then
  if [[ $runCode == 'launch' ]]; then
    export nLap=$nLapLong
  elif [[ $runCode == 'runserial' ]]; then
    export nLap=$nLapLong
  fi
fi


export BNPYDataPrefs=" \
  --nLap $nLap \
  --nBatch $nBatch \
  --datasetName $dataName
  "
