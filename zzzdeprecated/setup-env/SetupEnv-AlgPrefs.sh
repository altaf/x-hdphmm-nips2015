if [[ -z $doFullPassBeforeMstep ]]; then
   doFullPassBeforeMstep=1
else
   echo '>>> doFullPassBeforeMstep '$doFullPassBeforeMstep'<<'
fi

BNPYAlgPrefs="\
  --limitMemoryLP 1 \
  --convergeThr 0.001 \
  --minLaps 100 \
  --doFullPassBeforeMstep $doFullPassBeforeMstep \
  "

if [[ -n $nTask ]]; then
  BNPYAlgPrefs="$BNPYAlgPrefs --nTask $nTask"
fi
if [[ -n $initBlockLen ]]; then
  BNPYAlgPrefs="$BNPYAlgPrefs --initBlockLen $initBlockLen"
fi

