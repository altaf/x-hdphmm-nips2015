for K in 50 100
do

for hmmKappa in 0 100
do

#for algName in bnpyHDPHMMmemo bnpyHDPHMMdelmerge bnpyHDPHMMstoch
for algName in foxHDPHMMsampler
#for algName in bnpyHDPHMMstoch
do

python LaunchRun.py \
  --dataName ToyARK13 \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 10 \
  --launchCode $1 \
  --nickName finalbaselines \
  
done
done
done
