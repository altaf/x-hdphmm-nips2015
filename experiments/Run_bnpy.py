#!/contrib/projects/EnthoughtPython/epd64/bin/python
#$ -cwd
#$ -S /bin/bash
#$ -o /data/liv/xdump/logs/$JOB_ID.$TASK_ID.out
#$ -e /data/liv/xdump/logs/$JOB_ID.$TASK_ID.err
#$ -v PATH -v HOME -v SHELL
'''
 User-facing executable to do learning on the grid at Brown CS
'''

import sys
import os
import platform
import commands
import numpy as np
import argparse
import bnpy

class UnbufferedLogFile(object):
  def __init__(self, fileobj):
    # reopen stdout file descriptor with write mode
    # and 0 as the buffer size (unbuffered)
    self.file = os.fdopen( fileobj.fileno(), 'w', 0)  

  def flush( self ):
    self.file.flush()

  def __getattr__(self, attr):
    return getattr( self.file, attr )

  def write( self, data):
    self.file.write( data )
    self.file.flush()
    os.fsync( self.file.fileno() )
 
  def fileno( self ):
    return self.file.fileno()

  def close( self ):
    self.file.close()

def run(**kwargs):
  # Initial reconfiguration of stdout/stderr for grid
  if not sys.stdout.isatty():
    sys.stdout = UnbufferedLogFile(sys.stdout)
    sys.stderr = UnbufferedLogFile(sys.stderr)
    doWriteStdOut=False
    sys.path[0] = os.getcwd()

  jobID = '1'
  taskID = '1'
  dataName = None

  print '<<<<<<<<<<< This is Run_bnpy.py'
  print 'Input Args -----------'
  print '  ', ' '.join(sys.argv[1:4]), '...'
  for i in xrange(1, len(sys.argv), 2):
      key = sys.argv[i]
      if key.count('jobname'):
          jobname = sys.argv[i+1]
          print 'jobname:'
          print jobname
      if key.count('taskid') or key.count('taskID'):
          taskID = sys.argv[i+1]
      if key.count('dataName'):
          dataName = sys.argv[i+1]
          print 'dataName: ', dataName

  doWriteStdOut=True
  if not sys.stdout.isatty():
    try:
      jobID = os.getenv('JOB_ID')
      taskID = os.getenv('SGE_TASK_ID')
    except TypeError, ValueError:
      pass
       
    print 'Output Info ----------'
    try:
      print '  stdout: ', os.getenv('SGE_STDOUT_PATH')
      print '  stderr: ', os.getenv('SGE_STDERR_PATH')
    except:
      pass
    print 'Machine Info ---------'
    print '  Name: %s' % platform.node()
    CPUInfo = ( ''.join( commands.getoutput('cat /proc/cpuinfo') ) ).split('\n')
    CPUspeed = [l for l in CPUInfo if l.count('MHz')>0]
    CPUram   = [l for l in CPUInfo if l.count('cache size')>0]
    if len(CPUspeed) > 0:
      print '  CPU: %s MHz x%d' % (CPUspeed[0].split(':')[1], len(CPUspeed) )
    if len(CPUram) > 0:
      print '  RAM: %s x%d' % (CPUram[0].split(':')[1], len(CPUram))
    try:
      print '  Requested Num Cores: %s' % (os.environ['OMP_NUM_THREADS'])
    except KeyError:
      pass
    print 'Python Info ---------'
    print '  Python version %d.%d.%d' % sys.version_info[ :3]
    print '  Numpy version %s' % (np.__version__)
    print '  Cur Dir:', os.getcwd()
    print '  Local search path:', sys.path[0]

  print 'Job   Info ---------'
  print 'JobID  %s' % (jobID)
  print 'TaskID %s' % (taskID)
  print '---------------------------------------------'
  bnpy.Run.run(taskID=int(taskID), doWriteStdOut=doWriteStdOut, **kwargs)

if __name__ == '__main__':
  run()

