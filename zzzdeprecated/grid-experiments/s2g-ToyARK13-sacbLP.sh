## Script for launching jobs on the grid.
# Usage
# --------------
# s2g____.sh <no args>
#    print out list of jobs that will be run
# s2g____.sh launch
#    send all jobs to the grid

dataName='ToyARK13'
export jobName='Sunday'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=short
NTASK=1-10

export aModel=HDPHMM
export obsModel=AutoRegGauss
export ECovMat=eye
export sF=0.1
export nBatch=10
export initname=sacbLP
export K=-1
export learnRate=aa

for initBlockLen in 20 #35
do
  export initBlockLen=$initBlockLen
  echo "----------------------------- initBlockLen $initBlockLen"

# Remember: Need to run with fixed before sampler
for alg in stoch fixed delmerge #sampler #merge delmerge
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
exit
