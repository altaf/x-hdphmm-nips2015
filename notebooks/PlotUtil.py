import bnpy
import numpy as np
import os

from matplotlib import pylab
rcParams = pylab.rcParams
rcParams['pdf.fonttype'] = 42  # Make fonts export as text (not bitmap)
rcParams['ps.fonttype'] = 42  # Make fonts export as text (not bitmap)
rcParams['text.usetex'] = False
rcParams['axes.titlesize'] = 28
rcParams['axes.labelsize'] = 22
rcParams['legend.fontsize'] = 16
rcParams['xtick.labelsize'] = 16
rcParams['ytick.labelsize'] = 16
rcParams['figure.figsize'] = (4, 3.5)
rcParams['figure.subplot.left'] = 0.05
rcParams['figure.subplot.right'] = 0.95
rcParams['figure.subplot.bottom'] = 0.05
rcParams['figure.subplot.top'] = 0.95
rcParams['figure.subplot.wspace'] = 0
tickfontsize=18
W = 3
H = 3

def setDataName(dName):
  global dataName
  dataName = dName

def setUp(dName, ZColorMap, LineCMap, LineSMap,
      iKlims, iKticks, iLaplims, iLapticks, iHlims, iHticks,
      iELBOlims, iELBOticks,
      iZW, iZH,
      ):
  global dataName, StateColorMap, LineColorMap, LineStyleMap
  dataName = dName
  StateColorMap = ZColorMap
  LineColorMap = LineCMap
  LineStyleMap = LineSMap
  global Klims, Kticks, Laplims, Lapticks, Hlims, Hticks, ELBOlims, ELBOticks
  ELBOlims = iELBOlims
  ELBOticks = iELBOticks
  Klims = iKlims
  Kticks = iKticks
  Laplims = np.asarray(iLaplims)
  Lapticks =  np.asarray(iLapticks)
  Hlims = iHlims
  Hticks = iHticks
  global ZW, ZH
  ZW = iZW
  ZH = iZH

#################################################
#################################################

def filterJDictForRunsWithELBO(JDict):
    paths = JDict.values()
    names = JDict.keys()
    nSkip = 0
    badIDs = list()
    for ii, name in enumerate(names):
        if name.count('sampler'):
            ELBOpath = paths[ii] + "/1/evidence-saved-params.txt"
            if not os.path.exists(MakePath(ELBOpath)):
                nSkip += 1
                badIDs.append(ii)
    for ii in reversed(sorted(badIDs)):
        names.pop(ii)
        paths.pop(ii)
    if nSkip > 0:
        print 'SKIPPED %d jobnames related to sampler' % (nSkip)
    return names, paths

def plotELBO(JDict, xscale='linear', n='', **kwargs):
  names, paths = filterJDictForRunsWithELBO(JDict)
  bnpy.viz.PlotTrace.plotJobs(MakePaths(paths,n), names, MakeStyles(names),
                                     yvar='evidence', tickfontsize=tickfontsize,
                                     density=1, **kwargs)
  set_xscale(xscale)
  if ELBOlims is not None:
      pylab.ylim(ELBOlims);
  if ELBOticks is not None:
      pylab.yticks(ELBOticks);
      if np.max(np.abs(ELBOticks)) < 0.1:
          print 'Amplifying tick labels by 100'
          pylab.gca().set_yticklabels(100 * np.asarray(ELBOticks))
          pylab.ylabel('objective (x100)')
  pylab.gca().yaxis.grid() # horizontal lines
  pylab.gcf().set_size_inches(W, H);

def plotK(JDict, xscale='linear', n='', **kwargs):
  paths = JDict.values()
  names = JDict.keys()
  bnpy.viz.PlotTrace.plotJobs(MakePaths(paths,n), names, MakeStyles(names),
                                     yvar='K', tickfontsize=tickfontsize,
                                     density=1, **kwargs)
  set_xscale(xscale)
  pylab.ylim(Klims); pylab.yticks(Kticks);
  pylab.gca().yaxis.grid() # horizontal lines
  pylab.gcf().set_size_inches(W, H);

def plotKeff(JDict, xscale='linear', n='', **kwargs):
  paths = JDict.values()
  names = JDict.keys()
  bnpy.viz.PlotTrace.plotJobs(MakePaths(paths,n), names, MakeStyles(names),
                                     yvar='Keff', tickfontsize=tickfontsize,
                                     density=1, **kwargs)
  set_xscale(xscale)
  pylab.ylim(Klims); pylab.yticks(Kticks);
  pylab.gca().yaxis.grid() # horizontal lines
  pylab.gcf().set_size_inches(W, H);

def plotHammingDist(JDict, n='', xscale='linear', **kwargs):
  paths = JDict.values()
  names = JDict.keys()
  bnpy.viz.PlotTrace.plotJobs(MakePaths(paths,n), names, MakeStyles(names),
                                     yvar='hamming-distance', 
                                     tickfontsize=tickfontsize, 
                                     density=1, **kwargs)
  set_xscale(xscale)
  pylab.ylim(Hlims); 
  pylab.yticks(Hticks);
  pylab.gca().yaxis.grid() # horizontal lines
  pylab.gcf().set_size_inches(W, H);

def set_xscale(xscale):
  if xscale == 'linear':
    pylab.xlim(Laplims); pylab.xticks(Lapticks);
  else:
    pylab.gca().set_xscale('log', basex=2)
    pylab.xlim([0.5, Laplims[-1]])
    pylab.gca().set_xticks(np.maximum(Lapticks,1))
    pylab.gca().set_xticklabels(Lapticks)

def plotHammingDistVsELBO(JDict, n='', **kwargs):
  names, paths = filterJDictForRunsWithELBO(JDict)
  bnpy.viz.PlotTrace.plotJobs(MakePaths(paths, n), names, MakeStyles(names),
                                     yvar='hamming-distance',
                                     xvar='evidence', 
                                     tickfontsize=tickfontsize, 
                                     density=1, **kwargs)
  pylab.ylim(Hlims); 
  pylab.yticks(Hticks);
  pylab.gcf().set_size_inches(W, H);


def plotParamComparison(jPattern, n='', **kwargs):
  Info = bnpy.viz.PlotParamComparison.plotManyPanelsByPVar(
      MakePath(jPattern,n), doShowNow=1, **kwargs)
  pylab.gcf().set_size_inches(1 * W * Info['ncols'], H * Info['nrows']);
  pylab.tight_layout()


def plotComps(path, taskid=None, taskids=None, **kwargs):
  if isinstance(taskid, str) or isinstance(taskid, int):
    taskids = [taskid]
  bnpy.viz.PlotComps.plotCompsForJob(MakePath(path), 
                                            taskids=taskids, **kwargs)


def plotStateSeq(jobname, showELBOInTitle=1, xticks=None, **kwargs):
  global dataName, StateColorMap
  if 'cmap' not in kwargs:
      kwargs['cmap'] = StateColorMap
  axes, zBySeq = bnpy.viz.SequenceViz.plotSingleJob(dataName, jobname,
      showELBOInTitle=showELBOInTitle, **kwargs)
  pylab.subplots_adjust(top=0.85, bottom=0.1);
  axes[-1].tick_params(axis='both', which='major', labelsize=20)
  if xticks is not None:
      axes[-1].set_xticks(xticks);
  pylab.gcf().set_size_inches(ZW, ZH);
  pylab.draw();
  return axes


def MakeStyles(legnames):
  styleList = list()
  for name in legnames:
    lineType = None
    color = None
    for key in LineColorMap.keys():
      if name.count(key):
        color = LineColorMap[key]
    for key in LineStyleMap:
      if name.count(key):
        lineType = LineStyleMap[key]
    if lineType is None:
      print LineStyleMap.keys()
      raise ValueError('LineStyle not found for name: %s' % (name))
    elif color is None:
      print LineColorMap.keys()
      raise ValueError('Color not found for name: %s' % (name))
    style = dict(color=color, lineType=lineType, markersize=10, linewidth=1.75)
    styleList.append(style)
  return styleList

def MakePaths(pathlist, n=''):
    return [MakePath(p,n) for p in pathlist]

def MakePath(p, n=''):
    global dataName
    if dataName.count('SpeakerDiar') and n == '':
        n = '1'
    return os.environ['BNPYOUTDIR'] + dataName + n + '/' + p


def getSubsetByName(JJ, name, keepName=0):
    ''' Filter provided key/path dict to get only key/vals that match keyword.

    Returns
    -------
    J : OrderedDict
    * keys are subset of JJ.keys()
    * values are subset of JJ.values()
    '''
    from collections import OrderedDict
    JJ2 = OrderedDict()
    for key, path in JJ.items():
        if key.count(name):
            if keepName:
                newkey = key
            else:
                newkey = key.replace(name, '')
            newkey = newkey.strip()
            JJ2[newkey] = path
    return JJ2
