for K in 4
do

for hmmKappa in 0
do

#for algName in bnpyHDPHMMmemo 
for algName in foxHDPHMMsampler
do

python LaunchRun.py \
  --dataName DDToyHMM \
  --K $K \
  --K_test $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 1 \
  --launchCode $1 \
  --nickName test \
  --nLap 5 \

done
done
done
