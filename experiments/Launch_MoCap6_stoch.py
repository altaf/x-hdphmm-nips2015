#!/usr/bin/python

import subprocess

CMD = """
python LaunchRun.py \
 --dataName MoCap6 \
 --K $K \
 --hmmKappa $hmmKappa \
 --algName $algName \
 --nickName nipsexperiment \
 --nTask 10 \
"""

for hmmKappa in [100]:
    for K in [30, 60]:
        for algName in ['bnpyHDPHMMstoch']:

            # Execute the command
            curCMD = CMD + ""
            curCMD = curCMD.replace('$K', str(K))
            curCMD = curCMD.replace("$algName", algName)
            curCMD = curCMD.replace("$hmmKappa", str(hmmKappa))
            proc = subprocess.Popen(curCMD.split(), shell=False)
            proc.wait()

