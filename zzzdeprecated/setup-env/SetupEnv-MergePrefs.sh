if [[ -z $mergeStartLap ]]; then
  mergeStartLap=1
fi

export BNPYMergePrefs=" \
  --mergePairSelection wholeELBObetter \
  --mergeStartLap $mergeStartLap \
  --mergePerLap 25 \
  --mergeNumStuckBeforeQuit 25 \
  --mergeScoreRefreshInterval 10 \
  "
