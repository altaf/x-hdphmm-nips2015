global dataName
dataName = 'Chr7'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [-2, 108]
  Kticks = [0, 25, 50, 100]

  Laplims = [0, 40]
  Lapticks = [1, 2, 8, 32]
  #Lapticks = [0, 10, 20, 30, 40, 50]

  Hlims = [-0.05, 0.75]
  Hticks = [0, 0.2, 0.4, 0.6]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5

  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks,
                 ZW, ZH)

## initname=truelabels
J = dict()
kappa0prefix = "Wed-alg=HDPHMMfixed-hmmKappa=0-lik=DiagGauss-ECovMat=covdata-sF=0.75-randcontigblocks-initBlockLen=100"
kappa300prefix = "Wed-alg=HDPHMMfixed-hmmKappa=300-lik=DiagGauss-ECovMat=covdata-sF=0.75-randcontigblocks-initBlockLen=100"

kappa0prkm = "Wed-alg=HDPHMMfixed-hmmKappa=0-lik=DiagGauss-ECovMat=covdata-sF=0.75-kmeans"
kappa300prkm = "Wed-alg=HDPHMMfixed-hmmKappa=300-lik=DiagGauss-ECovMat=covdata-sF=0.75-kmeans"

J['fixed-kappa0-rcontig-K25'] = kappa0prefix + "-Kinit=25-nBatch=20"
J['fixed-kappa0-rcontig-K50'] = kappa0prefix + "-Kinit=50-nBatch=20"
J['fixed-kappa0-rcontig-K100'] = kappa0prefix + "-Kinit=100-nBatch=20"

J['fixed-kappa300-rcontig-K25'] = kappa300prefix + "-Kinit=25-nBatch=20"
J['fixed-kappa300-rcontig-K50'] = kappa300prefix + "-Kinit=50-nBatch=20"
J['fixed-kappa300-rcontig-K100'] = kappa300prefix + "-Kinit=100-nBatch=20"

J['fixed-kappa0-kmeans-K25'] = kappa0prkm + "-Kinit=25-nBatch=20"
J['fixed-kappa0-kmeans-K50'] = kappa0prkm + "-Kinit=50-nBatch=20"
J['fixed-kappa0-kmeans-K100'] = kappa0prkm + "-Kinit=100-nBatch=20"

J['fixed-kappa300-kmeans-K25'] = kappa300prkm + "-Kinit=25-nBatch=20"
J['fixed-kappa300-kmeans-K50'] = kappa300prkm + "-Kinit=50-nBatch=20"
J['fixed-kappa300-kmeans-K100'] = kappa300prkm + "-Kinit=100-nBatch=20"


def getSubsetByName(JJ, name):
  JJ2 = OrderedDict()
  for key, path in JJ.items():
    if key.count(name):
      JJ2[key] = path
  return JJ2

extraAlgNames = ['delmerge', 'stoch']
extraAlgSuffix = ['-mergeStartLap=3', '']

def getJobPathsByKeys():
  global J
  JJ = copy.deepcopy(J)
  for key, path in JJ.items():  
    for ii, algName in enumerate(extraAlgNames):
      rkey = key.replace('fixed', algName)
      rpath = path.replace('fixed', algName)
      if not os.path.exists(PlotUtil.MakePath(rpath)):
        rpath_tmp = rpath + extraAlgSuffix[ii]
        rpath_tmp2 = rpath.replace('stoch', 'stoch-learnRate=aa')
        if os.path.exists(PlotUtil.MakePath(rpath_tmp)):
          rpath = rpath_tmp
        elif os.path.exists(PlotUtil.MakePath(rpath_tmp2)):
          rpath = rpath_tmp2
        else:
          print 'CANNOT FIND AUTO-GENERATED PATH: ', rpath_tmp
          continue
      JJ[rkey] = rpath

    if not os.path.exists(PlotUtil.MakePath(path)):
      print 'CANNOT FIND USER-SPECIFIED PATH: ', path
      del JJ[key]
  # Sort by keys
  return OrderedDict(reversed(sorted(JJ.items())))

def getColorMap():
  ColorMap = OrderedDict()
  ColorMap['fixed'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  ColorMap['truelabels'] = 'y'
  ColorMap['repeattrue'] = 'm'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('25', ':'), ('50', '--'), ('100', '-'),
                  ])
  return StyleMap
