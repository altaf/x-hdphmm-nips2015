## Script for launching jobs on the grid.
# Usage
# --------------
# s2g____.sh <no args>
#    print out list of jobs that will be run
# s2g____.sh launch
#    send all jobs to the grid

dataName='MoCap124'
export jobName='Friday'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=liv
NTASK=1-5

export aModel=HDPHMM
export obsModel=AutoRegGauss
export ECovMat=diagcovfirstdiff
export sF=0.5
export VMat=same
export MMat=eye
export nBatch=20
export deleteStartLap=5
export deleteFailLimit=15
export mergeStartLap=5
export initname=randcontigblocks
export initBlockLen=20

for hmmKappa in 300
do
  export hmmKappa=$hmmKappa

for K in 150 200 #100 150
do
  export initBlockLen=$initBlockLen
  echo "-------------------- hmmKappa $hmmKappa | K $K | initBlockLen $initBlockLen"

# Remember: Need to run with fixed before sampler
for alg in delmerge
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
exit
