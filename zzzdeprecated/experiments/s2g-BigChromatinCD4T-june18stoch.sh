hmmKappa=100
initname='randcontigblocks'

for algName in bnpyHDPHMMstoch #bnpyHDPHMMmemo bnpyHDPHMMdelmerge
do

for K in 50 100
do
 
python LaunchRun.py \
  --dataName BigChromatinCD4T \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 2 \
  --nickName june21 \
  $*

done
done
