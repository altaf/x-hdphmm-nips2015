# New: expanded delete max size to full dataset
# New: made contigblocks longer (size=50)

for hmmKappa in 100 #0
do

for creationProposalName in bisectExistingBlocks randBlocks
do

for algName in bnpyHDPHMMcreateanddestroy #delmerge bnpyHDPHMMmemo
do

for K in 1 50 #50 100
do
 
initname='randcontigblocks'

python LaunchRun.py \
  --dataName ChromatinCD4T \
  --K $K \
  --creationProposalName $creationProposalName \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 2 \
  --launchCode $1 \
  --nickName may28

done
done
done
done
