global dataName
dataName = 'MoCap124'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 208]
  Kticks = [50, 100, 150, 200]

  Laplims = [0, 1000]
  Lapticks = [2, 10, 100, 500]

  Hlims = [-0.05, 0.75]
  Hticks = [0, 0.2, 0.4, 0.6]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5

  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks,
                 ZW, ZH)

## initname=truelabels
J = dict()
prefix = 'Friday-alg=HDPHMMfixed-hmmKappa=300-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye-'

J['fixed-rcontigblocks-Kinit=100'] = prefix + 'randcontigblocks-initBlockLen=20-Kinit=100-nBatch=20'
J['fixed-rcontigblocks-Kinit=150'] = prefix + 'randcontigblocks-initBlockLen=20-Kinit=150-nBatch=20' # mistake made, Kinit > 100 is dropped from save name
J['fixed-rcontigblocks-Kinit=200'] = prefix + 'randcontigblocks-initBlockLen=20-Kinit=200-nBatch=20'

def getSubsetByName(JJ, name):
  JJ2 = OrderedDict()
  for key, path in JJ.items():
    if key.count(name):
      JJ2[key] = path
  return JJ2

extraAlgNames = ['delmerge']
extraAlgSuffix = ['-mergeStartLap=5']

def getJobPathsByKeys():
  global J
  JJ = copy.deepcopy(J)
  for key, path in JJ.items():  
    for ii, algName in enumerate(extraAlgNames):
      rkey = key.replace('fixed', algName)
      rpath = path.replace('fixed', algName)
      if not os.path.exists(PlotUtil.MakePath(rpath)):
        rpath_tmp = rpath + extraAlgSuffix[ii]
        rpath_tmp2 = rpath.replace('stoch', 'stoch-learnRate=aa')
        rpath_tmp3 = rpath.replace('Wed-alg=HDPHMMsampler',
                                   'Wed-draft-alg=HDPHMMsampler')
        if os.path.exists(PlotUtil.MakePath(rpath_tmp)):
          rpath = rpath_tmp
        elif os.path.exists(PlotUtil.MakePath(rpath_tmp2)):
          rpath = rpath_tmp2    
        elif os.path.exists(PlotUtil.MakePath(rpath_tmp3)):
          rpath = rpath_tmp3
        else:
          print 'CANNOT FIND AUTO-GENERATED PATH: ', rpath_tmp
          continue
      JJ[rkey] = rpath

    if not os.path.exists(PlotUtil.MakePath(path)):
      print 'CANNOT FIND USER-SPECIFIED PATH: ', path
      del JJ[key]
  # Sort by keys
  return OrderedDict(sorted(JJ.items()))

def getColorMap():
  ColorMap = OrderedDict()
  ColorMap['fixed'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  ColorMap['truelabels'] = 'y'
  ColorMap['repeattrue'] = 'm'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('8', '-'), ('16', ':'), ('32','-'), ('64', '--'),
                   ('100', '-'), ('150', '--'), ('200', ':'),
                   ('truelabels', '-'), ('repeat', ':'),
                  ])
  return StyleMap
