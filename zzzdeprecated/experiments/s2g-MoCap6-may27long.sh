for K in 1
do

for initname in repeattruelabels truelabels randcontigblocks
do

for hmmKappa in 100
do

for creationProposalName in randBlocks bisectExistingBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --doFullPassBeforeMstep 1 \
  --dataName MoCap6 \
  --obsModelName AutoRegGauss \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --creationProposalName $creationProposalName \
  --nTask 2 \
  --launchCode $1 \
  --nickName may27long \
  --creationLapDelim_early 50 \
  --creationLapDelim_late 150 \
  --nBatch 6 \
  --minLaps 200
  
done
done
done
done
done
