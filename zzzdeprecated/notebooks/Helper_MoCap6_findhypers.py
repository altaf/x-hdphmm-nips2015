global dataName
dataName = 'MoCap6'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

def setUp():
  Klims = [0, 68]
  Kticks = [20, 40, 60]

  Laplims = [0, 1400]
  Lapticks = [2, 10, 100, 1000]

  Hlims = [0.1, 0.8]
  Hticks = [0.2, 0.4, 0.6]
  ELBOlims = None
  ELBOticks = None
  # Size of state seq plots
  ZW = 12
  ZH = 4.5
  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  return makePPListMapFromJPattern(PlotUtil.MakePath('findhypers'))

BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet + GreenSet[::-1] + OrangeSet + PurpleSet[::-1]
    C = np.asarray(Colors)/255.0;
    L = 10
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    print C
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    return []

def getLineStyleMap():
    return []
